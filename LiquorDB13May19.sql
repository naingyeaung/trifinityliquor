USE [LiquorFinalDB]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 5/13/2019 6:48:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Counters]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Counters](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CounterType] [int] NOT NULL,
	[Count] [int] NOT NULL,
 CONSTRAINT [PK_Counters] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Liquors]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Liquors](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Code] [nvarchar](max) NOT NULL,
	[Country] [nvarchar](max) NULL,
	[Brand] [nvarchar](max) NULL,
	[Classification] [nvarchar](max) NULL,
	[LiquorTypeID] [uniqueidentifier] NOT NULL,
	[Composition] [nvarchar](max) NULL,
	[ChemicalName] [nvarchar](max) NULL,
	[UnitPrice] [int] NOT NULL,
	[PercentageForSale] [int] NOT NULL,
	[Remark] [nvarchar](max) NULL,
	[Barcode] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedOn] [datetime2](7) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[ModifiedOn] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Liquors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LiquorTypes]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LiquorTypes](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Prefix] [nvarchar](max) NOT NULL,
	[Remark] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedOn] [datetime2](7) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[ModifiedOn] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_LiquorTypes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MainStoreLiquors]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MainStoreLiquors](
	[ID] [uniqueidentifier] NOT NULL,
	[MainStoreID] [uniqueidentifier] NOT NULL,
	[LiquorID] [uniqueidentifier] NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_MainStoreLiquors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MainStores]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MainStores](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Address] [nvarchar](max) NULL,
	[Township] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[Remark] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedOn] [datetime2](7) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[ModifiedOn] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_MainStores] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MainStoreStockRecords]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MainStoreStockRecords](
	[ID] [uniqueidentifier] NOT NULL,
	[MainStoreID] [uniqueidentifier] NOT NULL,
	[LiquorID] [uniqueidentifier] NOT NULL,
	[StockDate] [datetime2](7) NOT NULL,
	[ExpiredDate] [datetime2](7) NULL,
	[Quantity] [int] NOT NULL,
	[UnitID] [uniqueidentifier] NULL,
	[QuantityInSmallestUnit] [int] NOT NULL,
	[UnitName] [nvarchar](max) NULL,
	[CreatedOn] [datetime2](7) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[ModifiedOn] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_MainStoreStockRecords] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderOutletLiquors]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderOutletLiquors](
	[ID] [uniqueidentifier] NOT NULL,
	[OrderID] [uniqueidentifier] NOT NULL,
	[LiquorID] [uniqueidentifier] NOT NULL,
	[UnitID] [uniqueidentifier] NOT NULL,
	[UnitPrice] [decimal](18, 2) NOT NULL,
	[Quantity] [int] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[SortOrder] [int] NOT NULL,
	[QuantityInSmallestUnit] [int] NOT NULL,
	[UnitName] [nvarchar](max) NULL,
 CONSTRAINT [PK_OrderOutletLiquors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[ID] [uniqueidentifier] NOT NULL,
	[OutletID] [uniqueidentifier] NOT NULL,
	[VoucherNo] [nvarchar](max) NOT NULL,
	[OrderDate] [datetime2](7) NOT NULL,
	[CFfee] [int] NULL,
	[Total] [int] NOT NULL,
	[Discount] [int] NOT NULL,
	[Tax] [int] NOT NULL,
	[Balance] [int] NOT NULL,
	[Remark] [nvarchar](max) NULL,
	[Status] [int] NOT NULL,
	[IsPaid] [bit] NOT NULL,
	[PaidDate] [datetime2](7) NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedOn] [datetime2](7) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[ModifiedOn] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OutletLiquors]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OutletLiquors](
	[ID] [uniqueidentifier] NOT NULL,
	[OutletID] [uniqueidentifier] NOT NULL,
	[LiquorID] [uniqueidentifier] NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_OutletLiquors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Outlets]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Outlets](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Address] [nvarchar](max) NULL,
	[Township] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[Remark] [nvarchar](max) NULL,
	[VoucherPrefix] [nvarchar](max) NULL,
	[VoucherCounter] [int] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedOn] [datetime2](7) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[ModifiedOn] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Outlets] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PackingUnits]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PackingUnits](
	[ID] [uniqueidentifier] NOT NULL,
	[LiquorID] [uniqueidentifier] NOT NULL,
	[UnitID] [uniqueidentifier] NOT NULL,
	[QuantityInParent] [int] NOT NULL,
	[PurchasedAmount] [decimal](18, 2) NOT NULL,
	[SaleAmount] [decimal](18, 2) NOT NULL,
	[UnitOrder] [int] NOT NULL,
 CONSTRAINT [PK_PackingUnits] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TransferLiquors]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransferLiquors](
	[ID] [uniqueidentifier] NOT NULL,
	[TransferRecordID] [uniqueidentifier] NOT NULL,
	[LiquorID] [uniqueidentifier] NOT NULL,
	[Quantity] [int] NOT NULL,
	[UnitID] [uniqueidentifier] NOT NULL,
	[QuantityInSmallestUnit] [int] NOT NULL,
	[UnitName] [nvarchar](max) NULL,
 CONSTRAINT [PK_TransferLiquors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TransferRecords]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransferRecords](
	[ID] [uniqueidentifier] NOT NULL,
	[MainStoreID] [uniqueidentifier] NOT NULL,
	[OutletID] [uniqueidentifier] NOT NULL,
	[TransferDate] [datetime2](7) NOT NULL,
	[CreatedOn] [datetime2](7) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[ModifiedOn] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_TransferRecords] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Units]    Script Date: 5/13/2019 6:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Units](
	[ID] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[UnitOrder] [int] NOT NULL,
	[Remark] [nvarchar](max) NULL,
	[IsDelete] [bit] NOT NULL,
	[CreatedOn] [datetime2](7) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[ModifiedOn] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Units] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'00000000000000_CreateIdentitySchema', N'2.1.1-rtm-30846')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190125063233_model migrates', N'2.1.1-rtm-30846')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190125063359_model migrates again', N'2.1.1-rtm-30846')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190125065910_AfterCloneInit', N'2.1.1-rtm-30846')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190125100231_Counter', N'2.1.1-rtm-30846')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190128045209_packingunit', N'2.1.1-rtm-30846')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190128050411_PackingLiquor', N'2.1.1-rtm-30846')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'ed18332e-84b1-49e4-9ec4-b132a39a92a6', N'admin@gmail.com', N'ADMIN@GMAIL.COM', N'admin@gmail.com', N'ADMIN@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEDAdNQWMu8l7t72uVLBxp0IF5M3L6m7f1vEcRDHBvu26J8qYM6oIzdncXuWAIof1Ag==', N'N5BIV4QR24MNX227V75NKQ5TMS4TWODI', N'a8355a35-17a3-4304-b3de-9e5606b2a5b6', NULL, 0, 0, CAST(N'2019-04-04T08:19:51.2066703+00:00' AS DateTimeOffset), 1, 0)
SET IDENTITY_INSERT [dbo].[Counters] ON 

INSERT [dbo].[Counters] ([ID], [CounterType], [Count]) VALUES (1, 1, 10)
INSERT [dbo].[Counters] ([ID], [CounterType], [Count]) VALUES (2, 2, 0)
SET IDENTITY_INSERT [dbo].[Counters] OFF
INSERT [dbo].[Liquors] ([ID], [Name], [Code], [Country], [Brand], [Classification], [LiquorTypeID], [Composition], [ChemicalName], [UnitPrice], [PercentageForSale], [Remark], [Barcode], [IsDelete], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'32a7640a-c03e-4be7-9c6e-08d6bd661a0d', N'Heniken', N'B000007', N'Germany', N'Myanmar', NULL, N'6d4ccb85-a84e-4146-979f-61c047d3fc91', NULL, NULL, 10000, 20, NULL, N'8992779071300', 0, CAST(N'2019-04-10T10:09:14.0807590' AS DateTime2), N'ed18332e-84b1-49e4-9ec4-b132a39a92a6', CAST(N'2019-04-10T10:09:13.9888175' AS DateTime2), NULL)
INSERT [dbo].[Liquors] ([ID], [Name], [Code], [Country], [Brand], [Classification], [LiquorTypeID], [Composition], [ChemicalName], [UnitPrice], [PercentageForSale], [Remark], [Barcode], [IsDelete], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'7df51f44-6f95-481f-5692-08d6be529cdd', N'Carlsberg', N'B000008', N'Ge', N'Carls', NULL, N'6d4ccb85-a84e-4146-979f-61c047d3fc91', N'7%', NULL, 15000, 20, NULL, N'80798798798', 0, CAST(N'2019-04-11T14:22:14.0000000' AS DateTime2), N'ed18332e-84b1-49e4-9ec4-b132a39a92a6', CAST(N'2019-05-13T17:43:38.6180156' AS DateTime2), N'ed18332e-84b1-49e4-9ec4-b132a39a92a6')
INSERT [dbo].[Liquors] ([ID], [Name], [Code], [Country], [Brand], [Classification], [LiquorTypeID], [Composition], [ChemicalName], [UnitPrice], [PercentageForSale], [Remark], [Barcode], [IsDelete], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'9d1bd019-c49b-4214-5693-08d6be529cdd', N'Chang', N'B000009', N'Myanmar', N'Ch', NULL, N'6d4ccb85-a84e-4146-979f-61c047d3fc91', N'7%', NULL, 15000, 10, NULL, N'HKFK7897968', 1, CAST(N'2019-04-11T14:23:18.7657088' AS DateTime2), N'ed18332e-84b1-49e4-9ec4-b132a39a92a6', CAST(N'2019-04-11T14:23:18.7538744' AS DateTime2), NULL)
INSERT [dbo].[Liquors] ([ID], [Name], [Code], [Country], [Brand], [Classification], [LiquorTypeID], [Composition], [ChemicalName], [UnitPrice], [PercentageForSale], [Remark], [Barcode], [IsDelete], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'e6d901df-4d7f-43de-5694-08d6be529cdd', N'Tuborg', N'B000010', N'Eng', N'E', NULL, N'6d4ccb85-a84e-4146-979f-61c047d3fc91', N'7%', NULL, 10000, 20, NULL, N'JKDSE131S', 1, CAST(N'2019-04-11T14:24:13.8056302' AS DateTime2), N'ed18332e-84b1-49e4-9ec4-b132a39a92a6', CAST(N'2019-04-11T14:24:13.7913968' AS DateTime2), NULL)
INSERT [dbo].[LiquorTypes] ([ID], [Name], [Prefix], [Remark], [IsDelete], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'6d4ccb85-a84e-4146-979f-61c047d3fc91', N'Beer', N'B', NULL, 0, CAST(N'2019-04-10T10:08:40.0297198' AS DateTime2), N'ed18332e-84b1-49e4-9ec4-b132a39a92a6', CAST(N'2019-04-10T10:08:40.0046057' AS DateTime2), NULL)
INSERT [dbo].[MainStoreLiquors] ([ID], [MainStoreID], [LiquorID], [Quantity]) VALUES (N'86046c6d-dc0c-419e-893f-232e00224283', N'bae213e5-046d-48a1-93c4-e569d89f6112', N'e6d901df-4d7f-43de-5694-08d6be529cdd', 120)
INSERT [dbo].[MainStoreLiquors] ([ID], [MainStoreID], [LiquorID], [Quantity]) VALUES (N'7a6d845d-a677-4434-b25d-8d00a1ea46ad', N'bae213e5-046d-48a1-93c4-e569d89f6112', N'7df51f44-6f95-481f-5692-08d6be529cdd', 48)
INSERT [dbo].[MainStoreLiquors] ([ID], [MainStoreID], [LiquorID], [Quantity]) VALUES (N'996ab66c-6fdd-471e-b05b-ede616c6b028', N'bae213e5-046d-48a1-93c4-e569d89f6112', N'32a7640a-c03e-4be7-9c6e-08d6bd661a0d', 20)
INSERT [dbo].[MainStores] ([ID], [Name], [Address], [Township], [City], [Remark], [IsDelete], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'bae213e5-046d-48a1-93c4-e569d89f6112', N'LiquorHouse', NULL, NULL, NULL, NULL, 0, CAST(N'2019-01-28T11:40:16.5621992' AS DateTime2), N'ed18332e-84b1-49e4-9ec4-b132a39a92a6', CAST(N'2019-01-28T11:40:16.5439292' AS DateTime2), NULL)
INSERT [dbo].[MainStoreStockRecords] ([ID], [MainStoreID], [LiquorID], [StockDate], [ExpiredDate], [Quantity], [UnitID], [QuantityInSmallestUnit], [UnitName], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'0827985a-3eec-40a2-a62a-0086ac51bccb', N'bae213e5-046d-48a1-93c4-e569d89f6112', N'7df51f44-6f95-481f-5692-08d6be529cdd', CAST(N'2019-05-09T00:00:00.0000000' AS DateTime2), NULL, 5, N'4caf5fb5-1e5c-436e-bb5f-a2dbed36918a', 60, N'Box', CAST(N'2019-05-09T13:15:42.3046729' AS DateTime2), N'ed18332e-84b1-49e4-9ec4-b132a39a92a6', CAST(N'2019-05-09T13:15:42.2838093' AS DateTime2), NULL)
INSERT [dbo].[MainStoreStockRecords] ([ID], [MainStoreID], [LiquorID], [StockDate], [ExpiredDate], [Quantity], [UnitID], [QuantityInSmallestUnit], [UnitName], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'724aaf7a-f252-4813-aacf-060ea290d0f3', N'bae213e5-046d-48a1-93c4-e569d89f6112', N'e6d901df-4d7f-43de-5694-08d6be529cdd', CAST(N'2019-05-09T00:00:00.0000000' AS DateTime2), NULL, 10, N'4caf5fb5-1e5c-436e-bb5f-a2dbed36918a', 120, N'Box', CAST(N'2019-05-09T13:15:51.9693920' AS DateTime2), N'ed18332e-84b1-49e4-9ec4-b132a39a92a6', CAST(N'2019-05-09T13:15:51.9689100' AS DateTime2), NULL)
INSERT [dbo].[MainStoreStockRecords] ([ID], [MainStoreID], [LiquorID], [StockDate], [ExpiredDate], [Quantity], [UnitID], [QuantityInSmallestUnit], [UnitName], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'38a669a8-7b9a-4f5b-943c-93ce126951da', N'bae213e5-046d-48a1-93c4-e569d89f6112', N'32a7640a-c03e-4be7-9c6e-08d6bd661a0d', CAST(N'2019-04-10T00:00:00.0000000' AS DateTime2), NULL, 5, N'4caf5fb5-1e5c-436e-bb5f-a2dbed36918a', 50, N'Box', CAST(N'2019-04-10T10:09:41.2562465' AS DateTime2), N'ed18332e-84b1-49e4-9ec4-b132a39a92a6', CAST(N'2019-04-10T10:09:41.1824877' AS DateTime2), NULL)
INSERT [dbo].[OrderOutletLiquors] ([ID], [OrderID], [LiquorID], [UnitID], [UnitPrice], [Quantity], [Amount], [SortOrder], [QuantityInSmallestUnit], [UnitName]) VALUES (N'd110c3f5-c8ff-444f-28a0-08d6bd7c7a7f', N'e3d5d7f1-90c4-4205-afa1-4352f20f2506', N'32a7640a-c03e-4be7-9c6e-08d6bd661a0d', N'4caf5fb5-1e5c-436e-bb5f-a2dbed36918a', CAST(12000.00 AS Decimal(18, 2)), 1, CAST(12000.00 AS Decimal(18, 2)), 0, 10, NULL)
INSERT [dbo].[OrderOutletLiquors] ([ID], [OrderID], [LiquorID], [UnitID], [UnitPrice], [Quantity], [Amount], [SortOrder], [QuantityInSmallestUnit], [UnitName]) VALUES (N'2cba81f9-d5ae-4e38-28a1-08d6bd7c7a7f', N'052c4574-7262-43ee-9ff7-10f91943b642', N'32a7640a-c03e-4be7-9c6e-08d6bd661a0d', N'4caf5fb5-1e5c-436e-bb5f-a2dbed36918a', CAST(12000.00 AS Decimal(18, 2)), 1, CAST(12000.00 AS Decimal(18, 2)), 0, 10, NULL)
INSERT [dbo].[Orders] ([ID], [OutletID], [VoucherNo], [OrderDate], [CFfee], [Total], [Discount], [Tax], [Balance], [Remark], [Status], [IsPaid], [PaidDate], [IsDelete], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'052c4574-7262-43ee-9ff7-10f91943b642', N'1e4d11cd-2e89-4e3a-8922-e70a56ddec0b', N'000011', CAST(N'2019-04-10T00:00:00.0000000' AS DateTime2), NULL, 12000, 0, 0, 12000, NULL, 0, 0, NULL, 0, CAST(N'2019-04-10T12:54:58.9055927' AS DateTime2), N'ed18332e-84b1-49e4-9ec4-b132a39a92a6', CAST(N'2019-04-10T12:54:58.8998986' AS DateTime2), NULL)
INSERT [dbo].[Orders] ([ID], [OutletID], [VoucherNo], [OrderDate], [CFfee], [Total], [Discount], [Tax], [Balance], [Remark], [Status], [IsPaid], [PaidDate], [IsDelete], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'e3d5d7f1-90c4-4205-afa1-4352f20f2506', N'1e4d11cd-2e89-4e3a-8922-e70a56ddec0b', N'000010', CAST(N'2019-04-10T00:00:00.0000000' AS DateTime2), NULL, 12000, 0, 0, 0, NULL, 0, 0, NULL, 0, CAST(N'2019-04-10T12:49:24.7705361' AS DateTime2), N'ed18332e-84b1-49e4-9ec4-b132a39a92a6', CAST(N'2019-04-10T12:49:24.7042499' AS DateTime2), NULL)
INSERT [dbo].[OutletLiquors] ([ID], [OutletID], [LiquorID], [Quantity]) VALUES (N'00386b68-d8d8-443d-85d4-82ca5aa495e4', N'1e4d11cd-2e89-4e3a-8922-e70a56ddec0b', N'32a7640a-c03e-4be7-9c6e-08d6bd661a0d', 10)
INSERT [dbo].[OutletLiquors] ([ID], [OutletID], [LiquorID], [Quantity]) VALUES (N'6a011f79-3e54-4ce5-8590-8365efd290a5', N'1e4d11cd-2e89-4e3a-8922-e70a56ddec0b', N'7df51f44-6f95-481f-5692-08d6be529cdd', 12)
INSERT [dbo].[Outlets] ([ID], [Name], [Address], [Township], [City], [Remark], [VoucherPrefix], [VoucherCounter], [IsDelete], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'1e4d11cd-2e89-4e3a-8922-e70a56ddec0b', N'LiquorOutlet', N'2B', N'Hledan', N'Yangon', NULL, NULL, 11, 0, NULL, NULL, CAST(N'2019-01-28T14:27:18.0596276' AS DateTime2), NULL)
INSERT [dbo].[PackingUnits] ([ID], [LiquorID], [UnitID], [QuantityInParent], [PurchasedAmount], [SaleAmount], [UnitOrder]) VALUES (N'aafc7a3a-de9c-49cc-50d6-08d6bd661a14', N'32a7640a-c03e-4be7-9c6e-08d6bd661a0d', N'4caf5fb5-1e5c-436e-bb5f-a2dbed36918a', 1, CAST(10000.00 AS Decimal(18, 2)), CAST(12000.00 AS Decimal(18, 2)), 1)
INSERT [dbo].[PackingUnits] ([ID], [LiquorID], [UnitID], [QuantityInParent], [PurchasedAmount], [SaleAmount], [UnitOrder]) VALUES (N'0bb59f8a-2db4-45d9-50d7-08d6bd661a14', N'32a7640a-c03e-4be7-9c6e-08d6bd661a0d', N'bfd74e47-e80d-44c7-a0a5-ddc7590e097f', 10, CAST(1000.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), 2)
INSERT [dbo].[PackingUnits] ([ID], [LiquorID], [UnitID], [QuantityInParent], [PurchasedAmount], [SaleAmount], [UnitOrder]) VALUES (N'23f6a40e-4ce3-465b-d246-08d6be529cdf', N'9d1bd019-c49b-4214-5693-08d6be529cdd', N'4caf5fb5-1e5c-436e-bb5f-a2dbed36918a', 1, CAST(15000.00 AS Decimal(18, 2)), CAST(16500.00 AS Decimal(18, 2)), 1)
INSERT [dbo].[PackingUnits] ([ID], [LiquorID], [UnitID], [QuantityInParent], [PurchasedAmount], [SaleAmount], [UnitOrder]) VALUES (N'bb5a5baf-63f9-41db-d247-08d6be529cdf', N'9d1bd019-c49b-4214-5693-08d6be529cdd', N'bfd74e47-e80d-44c7-a0a5-ddc7590e097f', 12, CAST(1250.00 AS Decimal(18, 2)), CAST(1375.00 AS Decimal(18, 2)), 2)
INSERT [dbo].[PackingUnits] ([ID], [LiquorID], [UnitID], [QuantityInParent], [PurchasedAmount], [SaleAmount], [UnitOrder]) VALUES (N'73c259ee-e41d-4a86-d248-08d6be529cdf', N'e6d901df-4d7f-43de-5694-08d6be529cdd', N'4caf5fb5-1e5c-436e-bb5f-a2dbed36918a', 1, CAST(10000.00 AS Decimal(18, 2)), CAST(12000.00 AS Decimal(18, 2)), 1)
INSERT [dbo].[PackingUnits] ([ID], [LiquorID], [UnitID], [QuantityInParent], [PurchasedAmount], [SaleAmount], [UnitOrder]) VALUES (N'd469e1f8-2607-4455-d249-08d6be529cdf', N'e6d901df-4d7f-43de-5694-08d6be529cdd', N'bfd74e47-e80d-44c7-a0a5-ddc7590e097f', 12, CAST(833.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), 2)
INSERT [dbo].[PackingUnits] ([ID], [LiquorID], [UnitID], [QuantityInParent], [PurchasedAmount], [SaleAmount], [UnitOrder]) VALUES (N'9373c433-acbf-4369-fe28-08d6d793f601', N'7df51f44-6f95-481f-5692-08d6be529cdd', N'4caf5fb5-1e5c-436e-bb5f-a2dbed36918a', 1, CAST(15000.00 AS Decimal(18, 2)), CAST(18000.00 AS Decimal(18, 2)), 1)
INSERT [dbo].[PackingUnits] ([ID], [LiquorID], [UnitID], [QuantityInParent], [PurchasedAmount], [SaleAmount], [UnitOrder]) VALUES (N'7c1feec4-b4eb-4ab0-fe29-08d6d793f601', N'7df51f44-6f95-481f-5692-08d6be529cdd', N'bfd74e47-e80d-44c7-a0a5-ddc7590e097f', 12, CAST(1250.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), 2)
INSERT [dbo].[TransferLiquors] ([ID], [TransferRecordID], [LiquorID], [Quantity], [UnitID], [QuantityInSmallestUnit], [UnitName]) VALUES (N'5c62fc62-e989-4211-70bb-08d6bd6633a8', N'e38821db-7f10-43ba-8124-c3b4e38a0b69', N'32a7640a-c03e-4be7-9c6e-08d6bd661a0d', 2, N'4caf5fb5-1e5c-436e-bb5f-a2dbed36918a', 20, N'Box')
INSERT [dbo].[TransferLiquors] ([ID], [TransferRecordID], [LiquorID], [Quantity], [UnitID], [QuantityInSmallestUnit], [UnitName]) VALUES (N'b9356af0-522b-40c4-15f2-08d6d44aca37', N'360d6c84-6cb5-471c-8f47-c300c1f3b3a0', N'7df51f44-6f95-481f-5692-08d6be529cdd', 1, N'4caf5fb5-1e5c-436e-bb5f-a2dbed36918a', 12, N'Box')
INSERT [dbo].[TransferLiquors] ([ID], [TransferRecordID], [LiquorID], [Quantity], [UnitID], [QuantityInSmallestUnit], [UnitName]) VALUES (N'85eeafee-4ef0-4d36-15f3-08d6d44aca37', N'360d6c84-6cb5-471c-8f47-c300c1f3b3a0', N'32a7640a-c03e-4be7-9c6e-08d6bd661a0d', 1, N'4caf5fb5-1e5c-436e-bb5f-a2dbed36918a', 10, N'Box')
INSERT [dbo].[TransferRecords] ([ID], [MainStoreID], [OutletID], [TransferDate], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'360d6c84-6cb5-471c-8f47-c300c1f3b3a0', N'bae213e5-046d-48a1-93c4-e569d89f6112', N'1e4d11cd-2e89-4e3a-8922-e70a56ddec0b', CAST(N'2019-05-09T00:00:00.0000000' AS DateTime2), CAST(N'2019-05-09T13:21:40.4641631' AS DateTime2), N'ed18332e-84b1-49e4-9ec4-b132a39a92a6', CAST(N'2019-05-09T13:21:40.3547324' AS DateTime2), NULL)
INSERT [dbo].[TransferRecords] ([ID], [MainStoreID], [OutletID], [TransferDate], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'e38821db-7f10-43ba-8124-c3b4e38a0b69', N'bae213e5-046d-48a1-93c4-e569d89f6112', N'1e4d11cd-2e89-4e3a-8922-e70a56ddec0b', CAST(N'2019-04-10T00:00:00.0000000' AS DateTime2), CAST(N'2019-04-10T10:09:57.0396449' AS DateTime2), N'ed18332e-84b1-49e4-9ec4-b132a39a92a6', CAST(N'2019-04-10T10:09:56.9541643' AS DateTime2), NULL)
INSERT [dbo].[Units] ([ID], [Description], [UnitOrder], [Remark], [IsDelete], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'dec0227b-bdca-4600-b5ae-24a231f9f982', N'Can', 2, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Units] ([ID], [Description], [UnitOrder], [Remark], [IsDelete], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'4caf5fb5-1e5c-436e-bb5f-a2dbed36918a', N'Box', 1, NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Units] ([ID], [Description], [UnitOrder], [Remark], [IsDelete], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (N'bfd74e47-e80d-44c7-a0a5-ddc7590e097f', N'Bottle', 2, NULL, 0, NULL, NULL, NULL, NULL)
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Liquors]  WITH CHECK ADD  CONSTRAINT [FK_Liquors_LiquorTypes_LiquorTypeID] FOREIGN KEY([LiquorTypeID])
REFERENCES [dbo].[LiquorTypes] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Liquors] CHECK CONSTRAINT [FK_Liquors_LiquorTypes_LiquorTypeID]
GO
ALTER TABLE [dbo].[MainStoreLiquors]  WITH CHECK ADD  CONSTRAINT [FK_MainStoreLiquors_Liquors_LiquorID] FOREIGN KEY([LiquorID])
REFERENCES [dbo].[Liquors] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MainStoreLiquors] CHECK CONSTRAINT [FK_MainStoreLiquors_Liquors_LiquorID]
GO
ALTER TABLE [dbo].[MainStoreLiquors]  WITH CHECK ADD  CONSTRAINT [FK_MainStoreLiquors_MainStores_MainStoreID] FOREIGN KEY([MainStoreID])
REFERENCES [dbo].[MainStores] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MainStoreLiquors] CHECK CONSTRAINT [FK_MainStoreLiquors_MainStores_MainStoreID]
GO
ALTER TABLE [dbo].[MainStoreStockRecords]  WITH CHECK ADD  CONSTRAINT [FK_MainStoreStockRecords_Liquors_LiquorID] FOREIGN KEY([LiquorID])
REFERENCES [dbo].[Liquors] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MainStoreStockRecords] CHECK CONSTRAINT [FK_MainStoreStockRecords_Liquors_LiquorID]
GO
ALTER TABLE [dbo].[MainStoreStockRecords]  WITH CHECK ADD  CONSTRAINT [FK_MainStoreStockRecords_MainStores_MainStoreID] FOREIGN KEY([MainStoreID])
REFERENCES [dbo].[MainStores] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MainStoreStockRecords] CHECK CONSTRAINT [FK_MainStoreStockRecords_MainStores_MainStoreID]
GO
ALTER TABLE [dbo].[MainStoreStockRecords]  WITH CHECK ADD  CONSTRAINT [FK_MainStoreStockRecords_Units_UnitID] FOREIGN KEY([UnitID])
REFERENCES [dbo].[Units] ([ID])
GO
ALTER TABLE [dbo].[MainStoreStockRecords] CHECK CONSTRAINT [FK_MainStoreStockRecords_Units_UnitID]
GO
ALTER TABLE [dbo].[OrderOutletLiquors]  WITH CHECK ADD  CONSTRAINT [FK_OrderOutletLiquors_Liquors_LiquorID] FOREIGN KEY([LiquorID])
REFERENCES [dbo].[Liquors] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderOutletLiquors] CHECK CONSTRAINT [FK_OrderOutletLiquors_Liquors_LiquorID]
GO
ALTER TABLE [dbo].[OrderOutletLiquors]  WITH CHECK ADD  CONSTRAINT [FK_OrderOutletLiquors_Orders_OrderID] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Orders] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderOutletLiquors] CHECK CONSTRAINT [FK_OrderOutletLiquors_Orders_OrderID]
GO
ALTER TABLE [dbo].[OrderOutletLiquors]  WITH CHECK ADD  CONSTRAINT [FK_OrderOutletLiquors_Units_UnitID] FOREIGN KEY([UnitID])
REFERENCES [dbo].[Units] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderOutletLiquors] CHECK CONSTRAINT [FK_OrderOutletLiquors_Units_UnitID]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Outlets_OutletID] FOREIGN KEY([OutletID])
REFERENCES [dbo].[Outlets] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Outlets_OutletID]
GO
ALTER TABLE [dbo].[OutletLiquors]  WITH CHECK ADD  CONSTRAINT [FK_OutletLiquors_Liquors_LiquorID] FOREIGN KEY([LiquorID])
REFERENCES [dbo].[Liquors] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OutletLiquors] CHECK CONSTRAINT [FK_OutletLiquors_Liquors_LiquorID]
GO
ALTER TABLE [dbo].[OutletLiquors]  WITH CHECK ADD  CONSTRAINT [FK_OutletLiquors_Outlets_OutletID] FOREIGN KEY([OutletID])
REFERENCES [dbo].[Outlets] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OutletLiquors] CHECK CONSTRAINT [FK_OutletLiquors_Outlets_OutletID]
GO
ALTER TABLE [dbo].[PackingUnits]  WITH CHECK ADD  CONSTRAINT [FK_PackingUnits_Liquors_LiquorID] FOREIGN KEY([LiquorID])
REFERENCES [dbo].[Liquors] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PackingUnits] CHECK CONSTRAINT [FK_PackingUnits_Liquors_LiquorID]
GO
ALTER TABLE [dbo].[PackingUnits]  WITH CHECK ADD  CONSTRAINT [FK_PackingUnits_Units_UnitID] FOREIGN KEY([UnitID])
REFERENCES [dbo].[Units] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PackingUnits] CHECK CONSTRAINT [FK_PackingUnits_Units_UnitID]
GO
ALTER TABLE [dbo].[TransferLiquors]  WITH CHECK ADD  CONSTRAINT [FK_TransferLiquors_Liquors_LiquorID] FOREIGN KEY([LiquorID])
REFERENCES [dbo].[Liquors] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TransferLiquors] CHECK CONSTRAINT [FK_TransferLiquors_Liquors_LiquorID]
GO
ALTER TABLE [dbo].[TransferLiquors]  WITH CHECK ADD  CONSTRAINT [FK_TransferLiquors_TransferRecords_TransferRecordID] FOREIGN KEY([TransferRecordID])
REFERENCES [dbo].[TransferRecords] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TransferLiquors] CHECK CONSTRAINT [FK_TransferLiquors_TransferRecords_TransferRecordID]
GO
ALTER TABLE [dbo].[TransferLiquors]  WITH CHECK ADD  CONSTRAINT [FK_TransferLiquors_Units_UnitID] FOREIGN KEY([UnitID])
REFERENCES [dbo].[Units] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TransferLiquors] CHECK CONSTRAINT [FK_TransferLiquors_Units_UnitID]
GO
ALTER TABLE [dbo].[TransferRecords]  WITH CHECK ADD  CONSTRAINT [FK_TransferRecords_MainStores_MainStoreID] FOREIGN KEY([MainStoreID])
REFERENCES [dbo].[MainStores] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TransferRecords] CHECK CONSTRAINT [FK_TransferRecords_MainStores_MainStoreID]
GO
ALTER TABLE [dbo].[TransferRecords]  WITH CHECK ADD  CONSTRAINT [FK_TransferRecords_Outlets_OutletID] FOREIGN KEY([OutletID])
REFERENCES [dbo].[Outlets] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TransferRecords] CHECK CONSTRAINT [FK_TransferRecords_Outlets_OutletID]
GO
