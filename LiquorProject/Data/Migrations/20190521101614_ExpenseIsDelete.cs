﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LiquorProject.Data.Migrations
{
    public partial class ExpenseIsDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Expenses",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Expenses");
        }
    }
}
