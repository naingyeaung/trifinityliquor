﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LiquorProject.Data.Migrations
{
    public partial class modelmigratesagain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LiquorTypes",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Prefix = table.Column<string>(nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    IsDelete = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LiquorTypes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "MainStores",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Township = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Remark = table.Column<string>(nullable: true),
                    IsDelete = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainStores", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Outlets",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Township = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Remark = table.Column<string>(nullable: true),
                    VoucherPrefix = table.Column<string>(nullable: true),
                    VoucherCounter = table.Column<int>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Outlets", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Units",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    UnitOrder = table.Column<int>(nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    IsDelete = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Liquors",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Code = table.Column<string>(nullable: false),
                    Country = table.Column<string>(nullable: true),
                    Brand = table.Column<string>(nullable: true),
                    Classification = table.Column<string>(nullable: true),
                    LiquorTypeID = table.Column<Guid>(nullable: false),
                    Composition = table.Column<string>(nullable: true),
                    ChemicalName = table.Column<string>(nullable: true),
                    UnitPrice = table.Column<int>(nullable: false),
                    PercentageForSale = table.Column<int>(nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    Barcode = table.Column<string>(nullable: true),
                    IsDelete = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Liquors", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Liquors_LiquorTypes_LiquorTypeID",
                        column: x => x.LiquorTypeID,
                        principalTable: "LiquorTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    OutletID = table.Column<Guid>(nullable: false),
                    VoucherNo = table.Column<string>(nullable: false),
                    OrderDate = table.Column<DateTime>(nullable: false),
                    CFfee = table.Column<int>(nullable: true),
                    Total = table.Column<int>(nullable: false),
                    Discount = table.Column<int>(nullable: false),
                    Tax = table.Column<int>(nullable: false),
                    Balance = table.Column<int>(nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    IsPaid = table.Column<bool>(nullable: false),
                    PaidDate = table.Column<DateTime>(nullable: true),
                    IsDelete = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Orders_Outlets_OutletID",
                        column: x => x.OutletID,
                        principalTable: "Outlets",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransferRecords",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    MainStoreID = table.Column<Guid>(nullable: false),
                    OutletID = table.Column<Guid>(nullable: false),
                    TransferDate = table.Column<DateTime>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferRecords", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TransferRecords_MainStores_MainStoreID",
                        column: x => x.MainStoreID,
                        principalTable: "MainStores",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransferRecords_Outlets_OutletID",
                        column: x => x.OutletID,
                        principalTable: "Outlets",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MainStoreLiquors",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    MainStoreID = table.Column<Guid>(nullable: false),
                    LiquorID = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainStoreLiquors", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MainStoreLiquors_Liquors_LiquorID",
                        column: x => x.LiquorID,
                        principalTable: "Liquors",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MainStoreLiquors_MainStores_MainStoreID",
                        column: x => x.MainStoreID,
                        principalTable: "MainStores",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MainStoreStockRecords",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    MainStoreID = table.Column<Guid>(nullable: false),
                    LiquorID = table.Column<Guid>(nullable: false),
                    StockDate = table.Column<DateTime>(nullable: false),
                    ExpiredDate = table.Column<DateTime>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    UnitID = table.Column<Guid>(nullable: true),
                    QuantityInSmallestUnit = table.Column<int>(nullable: false),
                    UnitName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainStoreStockRecords", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MainStoreStockRecords_Liquors_LiquorID",
                        column: x => x.LiquorID,
                        principalTable: "Liquors",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MainStoreStockRecords_MainStores_MainStoreID",
                        column: x => x.MainStoreID,
                        principalTable: "MainStores",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MainStoreStockRecords_Units_UnitID",
                        column: x => x.UnitID,
                        principalTable: "Units",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OutletLiquors",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    OutletID = table.Column<Guid>(nullable: false),
                    LiquorID = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutletLiquors", x => x.ID);
                    table.ForeignKey(
                        name: "FK_OutletLiquors_Liquors_LiquorID",
                        column: x => x.LiquorID,
                        principalTable: "Liquors",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OutletLiquors_Outlets_OutletID",
                        column: x => x.OutletID,
                        principalTable: "Outlets",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PackingUnits",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    LiquorID = table.Column<Guid>(nullable: false),
                    UnitID = table.Column<Guid>(nullable: false),
                    QuantityInParent = table.Column<int>(nullable: false),
                    PurchasedAmount = table.Column<decimal>(nullable: false),
                    SaleAmount = table.Column<decimal>(nullable: false),
                    UnitOrder = table.Column<int>(nullable: false),
                    LiqourID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackingUnits", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PackingUnits_Liquors_LiqourID",
                        column: x => x.LiqourID,
                        principalTable: "Liquors",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PackingUnits_Units_UnitID",
                        column: x => x.UnitID,
                        principalTable: "Units",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderOutletLiquors",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    OrderID = table.Column<Guid>(nullable: false),
                    LiquorID = table.Column<Guid>(nullable: false),
                    UnitID = table.Column<Guid>(nullable: false),
                    UnitPrice = table.Column<decimal>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    SortOrder = table.Column<int>(nullable: false),
                    QuantityInSmallestUnit = table.Column<int>(nullable: false),
                    UnitName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderOutletLiquors", x => x.ID);
                    table.ForeignKey(
                        name: "FK_OrderOutletLiquors_Liquors_LiquorID",
                        column: x => x.LiquorID,
                        principalTable: "Liquors",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderOutletLiquors_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderOutletLiquors_Units_UnitID",
                        column: x => x.UnitID,
                        principalTable: "Units",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransferLiquors",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    TransferRecordID = table.Column<Guid>(nullable: false),
                    LiquorID = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    UnitID = table.Column<Guid>(nullable: false),
                    QuantityInSmallestUnit = table.Column<int>(nullable: false),
                    UnitName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferLiquors", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TransferLiquors_Liquors_LiquorID",
                        column: x => x.LiquorID,
                        principalTable: "Liquors",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransferLiquors_TransferRecords_TransferRecordID",
                        column: x => x.TransferRecordID,
                        principalTable: "TransferRecords",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransferLiquors_Units_UnitID",
                        column: x => x.UnitID,
                        principalTable: "Units",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Liquors_LiquorTypeID",
                table: "Liquors",
                column: "LiquorTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_MainStoreLiquors_LiquorID",
                table: "MainStoreLiquors",
                column: "LiquorID");

            migrationBuilder.CreateIndex(
                name: "IX_MainStoreLiquors_MainStoreID",
                table: "MainStoreLiquors",
                column: "MainStoreID");

            migrationBuilder.CreateIndex(
                name: "IX_MainStoreStockRecords_LiquorID",
                table: "MainStoreStockRecords",
                column: "LiquorID");

            migrationBuilder.CreateIndex(
                name: "IX_MainStoreStockRecords_MainStoreID",
                table: "MainStoreStockRecords",
                column: "MainStoreID");

            migrationBuilder.CreateIndex(
                name: "IX_MainStoreStockRecords_UnitID",
                table: "MainStoreStockRecords",
                column: "UnitID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderOutletLiquors_LiquorID",
                table: "OrderOutletLiquors",
                column: "LiquorID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderOutletLiquors_OrderID",
                table: "OrderOutletLiquors",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderOutletLiquors_UnitID",
                table: "OrderOutletLiquors",
                column: "UnitID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_OutletID",
                table: "Orders",
                column: "OutletID");

            migrationBuilder.CreateIndex(
                name: "IX_OutletLiquors_LiquorID",
                table: "OutletLiquors",
                column: "LiquorID");

            migrationBuilder.CreateIndex(
                name: "IX_OutletLiquors_OutletID",
                table: "OutletLiquors",
                column: "OutletID");

            migrationBuilder.CreateIndex(
                name: "IX_PackingUnits_LiqourID",
                table: "PackingUnits",
                column: "LiqourID");

            migrationBuilder.CreateIndex(
                name: "IX_PackingUnits_UnitID",
                table: "PackingUnits",
                column: "UnitID");

            migrationBuilder.CreateIndex(
                name: "IX_TransferLiquors_LiquorID",
                table: "TransferLiquors",
                column: "LiquorID");

            migrationBuilder.CreateIndex(
                name: "IX_TransferLiquors_TransferRecordID",
                table: "TransferLiquors",
                column: "TransferRecordID");

            migrationBuilder.CreateIndex(
                name: "IX_TransferLiquors_UnitID",
                table: "TransferLiquors",
                column: "UnitID");

            migrationBuilder.CreateIndex(
                name: "IX_TransferRecords_MainStoreID",
                table: "TransferRecords",
                column: "MainStoreID");

            migrationBuilder.CreateIndex(
                name: "IX_TransferRecords_OutletID",
                table: "TransferRecords",
                column: "OutletID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MainStoreLiquors");

            migrationBuilder.DropTable(
                name: "MainStoreStockRecords");

            migrationBuilder.DropTable(
                name: "OrderOutletLiquors");

            migrationBuilder.DropTable(
                name: "OutletLiquors");

            migrationBuilder.DropTable(
                name: "PackingUnits");

            migrationBuilder.DropTable(
                name: "TransferLiquors");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Liquors");

            migrationBuilder.DropTable(
                name: "TransferRecords");

            migrationBuilder.DropTable(
                name: "Units");

            migrationBuilder.DropTable(
                name: "LiquorTypes");

            migrationBuilder.DropTable(
                name: "MainStores");

            migrationBuilder.DropTable(
                name: "Outlets");
        }
    }
}
