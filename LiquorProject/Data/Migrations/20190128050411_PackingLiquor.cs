﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LiquorProject.Data.Migrations
{
    public partial class PackingLiquor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PackingUnits_Liquors_LiqourID",
                table: "PackingUnits");

            migrationBuilder.DropIndex(
                name: "IX_PackingUnits_LiqourID",
                table: "PackingUnits");

            migrationBuilder.DropColumn(
                name: "LiqourID",
                table: "PackingUnits");

            migrationBuilder.CreateIndex(
                name: "IX_PackingUnits_LiquorID",
                table: "PackingUnits",
                column: "LiquorID");

            migrationBuilder.AddForeignKey(
                name: "FK_PackingUnits_Liquors_LiquorID",
                table: "PackingUnits",
                column: "LiquorID",
                principalTable: "Liquors",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PackingUnits_Liquors_LiquorID",
                table: "PackingUnits");

            migrationBuilder.DropIndex(
                name: "IX_PackingUnits_LiquorID",
                table: "PackingUnits");

            migrationBuilder.AddColumn<Guid>(
                name: "LiqourID",
                table: "PackingUnits",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_PackingUnits_LiqourID",
                table: "PackingUnits",
                column: "LiqourID");

            migrationBuilder.AddForeignKey(
                name: "FK_PackingUnits_Liquors_LiqourID",
                table: "PackingUnits",
                column: "LiqourID",
                principalTable: "Liquors",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
