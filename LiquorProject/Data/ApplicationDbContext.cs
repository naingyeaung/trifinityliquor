﻿using System;
using System.Collections.Generic;
using System.Text;
using LiquorProject.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LiquorProject.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<LiquorType> LiquorTypes { get; set; }

        public DbSet<Liquor> Liquors { get; set; }

        public DbSet<MainStore> MainStores { get; set; }

        public DbSet<MainStoreLiquor> MainStoreLiquors { get; set; }

        public DbSet<MainStoreStockRecord> MainStoreStockRecords { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderOutletLiquor> OrderOutletLiquors { get; set; }

        public DbSet<Outlet> Outlets { get; set; }

        public DbSet<OutletLiquor> OutletLiquors { get; set; }

        public DbSet<PackingUnit> PackingUnits { get; set; }

        public DbSet<TransferLiquor> TransferLiquors { get; set; }

        public DbSet<TransferRecord> TransferRecords { get; set; }

        public DbSet<Unit> Units { get; set; }

        public DbSet<Counter> Counters { get; set; }

        public DbSet<ExpenseType> ExpenseTypes { get; set; }

        public DbSet<Expense> Expenses { get; set; }
    }
}
