﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class MainStoreLiquor
    {
        public Guid ID { get; set; }

        [Required]
        [DisplayName("Main Store")]
        public Guid MainStoreID { get; set; }

        [Required]
        [DisplayName("Liquor")]
        public Guid LiquorID { get; set; }

        [Required]
        public int Quantity { get; set; }

        // Foreign Key Tables
        [ForeignKey("MainStoreID")]
        public MainStore MainStore { get; set; }

        [ForeignKey("LiquorID")]
        public Liquor Liquor { get; set; }
    }
}
