﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class Unit
    {
        public Guid ID { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [DisplayName("Unit Order")]
        public int UnitOrder { get; set; }

        [DataType(DataType.MultilineText)]
        public string Remark { get; set; }

        [Required]
        public bool IsDelete { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }

        // Foreign Key Tables
        public ICollection<PackingUnit> PackingUnits { get; set; }

        public ICollection<OrderOutletLiquor> OrderOutletLiquors { get; set; }

        // Constructor
        public Unit()
        {
            IsDelete = false;

            ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
        }
    }
}
