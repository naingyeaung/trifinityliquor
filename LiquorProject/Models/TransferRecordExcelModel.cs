﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class TransferRecordExcelModel
    {
        public string No { get; set; }

        public string TransferDate { get; set; }

        public string Liquors { get; set; }

        public string ItemCount { get; set; }

        public string TotalQty { get; set; }
    }
}
