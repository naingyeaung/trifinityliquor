﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class Outlet
    {
        public Guid ID { get; set; }

        [Required]
        public string Name { get; set; }

        public string Address { get; set; }

        public string Township { get; set; }

        public string City { get; set; }

        [DataType(DataType.MultilineText)]
        public string Remark { get; set; }

        public string VoucherPrefix { get; set; }

        public int VoucherCounter { get; set; }

        [Required]
        public bool IsDelete { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }

        // Foreign Key Tables

        public ICollection<TransferRecord> TransferRecords { get; set; }

        public ICollection<OutletLiquor> OutletLiquors { get; set; }

        public ICollection<Order> Orders { get; set; }

        // Constructor
        public Outlet()
        {
            VoucherCounter = 0;

            IsDelete = false;

            ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
        }
    }
}
