﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class LiquorExcel
    {
        public string No { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Price { get; set; }
    }
}
