﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class Expense
    {
        public Guid ID { get; set; }

        public string Description { get; set; }

        public int Cost { get; set; }

        public Guid ExpenseTypeID { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        public DateTime ExpenseDate { get; set; }

        public string CreatedBy { get; set; }

        public bool IsDelete { get; set; }

        public string ModifiedBy { get; set; }

        //foreign key
        public ExpenseType ExpenseType { get; set; }

        public Expense()
        {

        }
    }
}
