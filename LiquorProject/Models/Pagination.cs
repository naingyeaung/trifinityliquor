﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class Pagination
    {
        public int Page { get; set; }

        public int PageSize { get; set; }
    }
}
