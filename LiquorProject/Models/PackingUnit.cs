﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class PackingUnit
    {
        public Guid ID { get; set; }

        [Required]
        public Guid LiquorID { get; set; }

        [Required]
        public Guid UnitID { get; set; }

        [Required]
        public int QuantityInParent { get; set; } // This shows how many unit in its parent container.

        [Required]
        public decimal PurchasedAmount { get; set; }

        [Required]
        public decimal SaleAmount { get; set; }

        [Required]
        public int UnitOrder { get; set; }

        public Liquor Liquor { get; set; }

        [ForeignKey("UnitID")]
        public Unit Unit { get; set; }

        // Constructor
        public PackingUnit()
        {
            QuantityInParent = 1;

            PurchasedAmount = 0;

            SaleAmount = 0;
        }
    }
}
