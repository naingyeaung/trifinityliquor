﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class OrderOutletLiquor
    {
        public Guid ID { get; set; }

        [Required]
        public Guid OrderID { get; set; }

        [Required]
        public Guid LiquorID { get; set; }

        [Required]
        public Guid UnitID { get; set; }

        [Required]
        public decimal UnitPrice { get; set; } // Unit Price for Packing Unit

        [Required]
        public int Quantity { get; set; }

        [Required]
        public decimal Amount { get; set; }

        public int SortOrder { get; set; }

        public int QuantityInSmallestUnit { get; set; }

        public string UnitName { get; set; }

        //public Guid OutletMedicineID { get; set; }

        // Foreign Key Tables
        public Order Order { get; set; }

        [ForeignKey("LiquorID")]
        public Liquor Liquor { get; set; }

        [ForeignKey("UnitID")]
        public Unit Unit { get; set; }

        //public OutletMedicine OutletMedicine { get; set; }

        public OrderOutletLiquor()
        {
            SortOrder = 0;

            UnitPrice = 0;

            Quantity = 0;

            Amount = UnitPrice * Quantity;
        }
    }
}
