﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class MainStoreStockRecord
    {
        public Guid ID { get; set; }

        [Required]
        [DisplayName("Main Store")]
        public Guid MainStoreID { get; set; }

        [Required]
        [DisplayName("Liquor")]
        public Guid LiquorID { get; set; }

        [Required]
        [DisplayName("Stock Date")]
        public DateTime StockDate { get; set; }

        [DisplayName("Expired Date")]
        public DateTime? ExpiredDate { get; set; }

        [Required]
        public int Quantity { get; set; }

        public Guid? UnitID { get; set; }

        public int QuantityInSmallestUnit { get; set; }

        public string UnitName { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }

        // Foreign Key Tables
        [ForeignKey("MainStoreID")]
        public MainStore MainStore { get; set; }

        [ForeignKey("LiquorID")]
        public Liquor Liquor { get; set; }

        [ForeignKey("UnitID")]
        public Unit Unit { get; set; }

        // Constructor
        public MainStoreStockRecord()
        {
            ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
        }
    }
}
