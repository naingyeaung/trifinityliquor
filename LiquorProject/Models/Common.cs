﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class Common
    {
        public static List<QtySet> ChangeMinQtyToQtySet(Liquor liquor, int realSmallestQty) // id = Medicine ID
        {
            var i = 0;
            List<QtySet> qtySet = new List<QtySet>();
            var smallestQty = realSmallestQty;
            var packingUnits = liquor.PackingUnits.OrderByDescending(x => x.UnitOrder);
            foreach (var packingUnit in packingUnits)
            {
                i++;
                var tempQty = smallestQty / packingUnit.QuantityInParent;
                if (i != packingUnits.Count())
                {
                    qtySet.Add(new QtySet(packingUnit.Unit.Description, (smallestQty % packingUnit.QuantityInParent), packingUnit.UnitOrder));
                    smallestQty = tempQty;
                }
                else
                {
                    qtySet.Add(new QtySet(packingUnit.Unit.Description, tempQty, packingUnit.UnitOrder));
                }
            }
            return qtySet;
        }

        public static string ChangeQtySetToString(List<QtySet> qtySet)
        {
            string qty = "";
            foreach (var set in qtySet.OrderBy(x => x.UnitOrder))
            {
                qty += qty == "" ? set.Quantity + set.Unit : ", " + set.Quantity + set.Unit;
            }
            return qty;
        }
    }

    public class QtySet
    {
        public string Unit { get; set; }
        public int Quantity { get; set; }
        public int UnitOrder { get; set; }

        public QtySet(string Unit, int Quantity, int UnitOrder)
        {
            this.Unit = Unit;
            this.Quantity = Quantity;
            this.UnitOrder = UnitOrder;
        }
    }
}