﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class TransferRecord
    {
        public Guid ID { get; set; }

        [Required]
        public Guid MainStoreID { get; set; }

        [Required]
        public Guid OutletID { get; set; }

        [Required]
        public DateTime TransferDate { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }

        // Foreign Key Tables
        [ForeignKey("MainStoreID")]
        public MainStore MainStore { get; set; }

        [ForeignKey("OutletID")]
        public Outlet Outlet { get; set; }

        [Required]
        public ICollection<TransferLiquor> TransferLiquors { get; set; }

        // Constructor
        public TransferRecord()
        {
            ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
        }
    }
}
