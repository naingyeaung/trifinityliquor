﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class TransferLiquor
    {
        //[Key]
        public Guid ID { get; set; }

        //[Key]
        public Guid TransferRecordID { get; set; }

        //[Key]
        public Guid LiquorID { get; set; }

        [Required]
        public int Quantity { get; set; }

        public Guid UnitID { get; set; }

        public int QuantityInSmallestUnit { get; set; }

        public string UnitName { get; set; }

        // Foreign Key Tables
        [ForeignKey("TransferRecordID")]
        public TransferRecord TransfarRecord { get; set; }

        [ForeignKey("LiquorID")]
        public Liquor Liquor { get; set; }

        [ForeignKey("UnitID")]
        public Unit Unit { get; set; }

        // Constructor
        public TransferLiquor()
        {
            Quantity = 0;
        }
    }
}
