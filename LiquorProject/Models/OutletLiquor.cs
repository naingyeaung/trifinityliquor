﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class OutletLiquor
    {
        public Guid ID { get; set; }

        [Required]
        [DisplayName("Main Store")]
        public Guid OutletID { get; set; }

        [Required]
        [DisplayName("Liquor")]
        public Guid LiquorID { get; set; }

        [Required]
        public int Quantity { get; set; }

        // Foreign Key Tables
        [ForeignKey("OutletID")]
        public Outlet Outlet { get; set; }

        [ForeignKey("LiquorID")]
        public Liquor Liquor { get; set; }

        //public ICollection<OrderOutletMedicine> OrderOutletMedicine { get; set; }
    }
}
