﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class MainStore
    {
        public Guid ID { get; set; }

        [Required]
        public string Name { get; set; }

        public string Address { get; set; }

        public string Township { get; set; }

        public string City { get; set; }

        [DataType(DataType.MultilineText)]
        public string Remark { get; set; }

        [Required]
        public bool IsDelete { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }

        // Foreign Key Tables
        public ICollection<MainStoreStockRecord> MainStoreStockRecords { get; set; }

        public ICollection<MainStoreLiquor> MainStoreLiquors { get; set; }

        public ICollection<TransferRecord> TransferRecords { get; set; }

        // Constructor
        public MainStore()
        {
            IsDelete = false;

            ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
        }
    }
}
