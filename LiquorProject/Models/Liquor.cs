﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class Liquor
    {
        public Guid ID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Code { get; set; }

        public string Country { get; set; }

        public string Brand { get; set; }

        public string Classification { get; set; }

        [Required]
        [DisplayName("Name")]
        public Guid LiquorTypeID { get; set; }

        public string Composition { get; set; }

        public string ChemicalName { get; set; }

        [Required]
        [DisplayName("Unit Price")]
        public int UnitPrice { get; set; }

        [Required]
        [DisplayName("Percentage For Sale")]
        public int PercentageForSale { get; set; }

        [DataType(DataType.MultilineText)]
        public string Remark { get; set; }

        public string Barcode { get; set; }

        [Required]
        public bool IsDelete { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }

        // Foreign Key Tables
        [ForeignKey("LiquorTypeID")]
        public LiquorType LiquorType { get; set; }

        [Required]
        public ICollection<PackingUnit> PackingUnits { get; set; }

        public ICollection<MainStoreStockRecord> MainStoreStockRecords { get; set; }

        public ICollection<MainStoreLiquor> MainStoreLiquors { get; set; }

        public ICollection<TransferLiquor> TransferLiquors { get; set; }

        public ICollection<OrderOutletLiquor> OrderOutletLiquors { get; set; }

        // Constructor
        public Liquor()
        {
            UnitPrice = 0;

            PercentageForSale = 0;

            IsDelete = false;

            ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
        }
    }
}
