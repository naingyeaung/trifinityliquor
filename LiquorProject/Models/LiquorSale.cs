﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class LiquorSale
    {
        public string No { get; set; }

        public string Name { get; set; }

        public string Count { get; set; }

        public decimal Cost { get; set; }

        public string CostInUnit { get; set; }
    }
}
