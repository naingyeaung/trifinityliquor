﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LiquorProject.Models
{
    public class Order
    {
        public Guid ID { get; set; }

        [Required]
        public Guid OutletID { get; set; }

        [Required]
        public string VoucherNo { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        public int? CFfee { get; set; }

        [Required]
        public int Total { get; set; }

        [Required]
        public int Discount { get; set; }

        public int Tax { get; set; }

        [Required]
        public int Balance { get; set; }


        [DataType(DataType.MultilineText)]
        public string Remark { get; set; }

        public int Status { get; set; }

        [Required]
        [DisplayName("Paid")]
        public bool IsPaid { get; set; }

        public DateTime? PaidDate { get; set; }

        [Required]
        public bool IsDelete { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }

        // Foreign Key Tables
        public Outlet Outlet { get; set; }

        public ICollection<OrderOutletLiquor> OrderOutletLiquors { get; set; }


        // Constructor
        public Order()
        {
            Total = 0;

            Discount = 0;

            Tax = 0;

            Balance = 0;

            Status = 0;

            IsPaid = false;

            IsDelete = false;

            ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
        }
    }
}
