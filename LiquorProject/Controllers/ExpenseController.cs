﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LiquorProject.Data;
using LiquorProject.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PagedList.Core;

namespace LiquorProject.Controllers
{
    public class ExpenseController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly int _page;
        private readonly int _pageSize;

        public ExpenseController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, Pagination pagination)
        {
            _userManager = userManager;
            _context = context;
            _page = pagination.Page;
            _pageSize = pagination.PageSize;
        }
        public IActionResult Index(int page = 0, int pageSize = 0)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }

            var expenses = _context.Expenses.Where(x => !x.IsDelete).OrderBy(x => x.ExpenseDate).Include(x => x.ExpenseType);

            page = page == 0 ? this._page : page;
            pageSize = pageSize == 0 ? this._pageSize : pageSize;

            var model = new PagedList<Expense>(expenses, page, pageSize);
            return View(model);
        }

        public IActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }

            ViewData["ExpenseTypeID"] = new SelectList(_context.ExpenseTypes.Where(x => !x.IsDelete), "ID", "Description");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Expense model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }

            model.ID = Guid.NewGuid();
            model.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            model.ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            model.CreatedBy = _userManager.GetUserId(User);
            model.ModifiedBy = _userManager.GetUserId(User);
            model.IsDelete = false;

            _context.Expenses.Add(model);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        public IActionResult Details(Guid id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }

            var expesne = _context.Expenses.Where(x => !x.IsDelete && x.ID == id).FirstOrDefault();
            ViewData["ExpenseTypeID"] = new SelectList(_context.ExpenseTypes.Where(x => !x.IsDelete), "ID", "Description");

            if (expesne != null)
            {
                return View(expesne);
            }

            return NotFound();
        }

        public IActionResult Delete(Guid id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }

            var expense = _context.Expenses.Where(x => !x.IsDelete && x.ID == id).Include(x => x.ExpenseType).FirstOrDefault();
            ViewData["ExpenseTypeID"] = new SelectList(_context.ExpenseTypes.Where(x => !x.IsDelete), "ID", "Description");

            if (expense != null)
            {
                return View(expense);
            }

            return NotFound();
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(Guid id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }

            var expense = _context.Expenses.Where(x => !x.IsDelete && x.ID == id).FirstOrDefault();

            if (expense != null)
            {
                expense.IsDelete = true;
                _context.SaveChanges();

                return RedirectToAction(nameof(Index));
            }

            return NotFound();
        }
    }

}