﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LiquorProject.Models;
using LiquorProject.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace LiquorProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly int _page;
        private readonly int _pageSize;
        public readonly IConfiguration _configuration;

        public HomeController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, Pagination pagination, IConfiguration configuration)
        {
            _userManager = userManager;
            _context = context;
            _page = pagination.Page;
            _pageSize = pagination.PageSize;
            _configuration = configuration;
        }
        public IActionResult Index()
        {

            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            else
            {
                var orders = _context.Orders.Where(x => (x.CreatedOn ?? DateTime.UtcNow.AddHours(6).AddMinutes(30)).Month == DateTime.UtcNow.AddHours(6).AddMinutes(30).Month && (x.CreatedOn ?? DateTime.UtcNow.AddHours(6).AddMinutes(30)).Year == DateTime.UtcNow.AddHours(6).AddMinutes(30).Year);


                var totalAmount = 0;
                var totalExpense = 0;

                totalExpense = _context.Expenses.Where(x => !x.IsDelete && x.ExpenseDate.Month == DateTime.UtcNow.AddHours(6).AddMinutes(30).Month && x.ExpenseDate.Year == DateTime.UtcNow.AddHours(6).AddMinutes(30).Year).Sum(x => x.Cost);

                foreach (var order in orders)
                {
                    totalAmount += order.Balance;
                }

                var dailyOrders = _context.Orders.Where(x => (x.CreatedOn ?? DateTime.UtcNow.AddHours(6).AddMinutes(30)).Date == DateTime.UtcNow.AddHours(6).AddMinutes(30).Date);


                var dailyTotalAmount = 0;

                var dailyTotalExpense = 0;

                dailyTotalExpense = _context.Expenses.Where(x => !x.IsDelete && x.ExpenseDate.Date == DateTime.UtcNow.AddHours(6).AddMinutes(30).Date).Sum(x => x.Cost);

                foreach (var order in dailyOrders)
                {
                    dailyTotalAmount += order.Balance;
                }

                ViewBag.MonthlyTotal = totalAmount;
                ViewBag.MonthlyExpense = totalExpense;
                ViewBag.DailyTotal = dailyTotalAmount;
                ViewBag.DailyExpense = dailyTotalExpense;
                return View();
            }

        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
