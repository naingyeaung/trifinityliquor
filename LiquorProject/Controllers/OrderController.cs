﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LiquorProject.Data;
using LiquorProject.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PagedList.Core;

namespace LiquorProject.Controllers
{
    public class OrderController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly int _page;
        private readonly int _pageSize;
        public readonly IConfiguration _configuration;

        public OrderController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, Pagination pagination, IConfiguration configuration)
        {
            _userManager = userManager;
            _context = context;
            _page = pagination.Page;
            _pageSize = pagination.PageSize;
            _configuration = configuration;
        }
        public IActionResult Index(string context = "", int page = 0, int pageSize = 0)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            page = page == 0 ? this._page : page;
            pageSize = pageSize == 0 ? this._pageSize : pageSize;
            page = page == 0 ? this._page : page;
            pageSize = pageSize == 0 ? this._pageSize : pageSize;
            var orders = _context.Orders.Where(x => !x.IsDelete && (string.IsNullOrEmpty(context) ? true : x.VoucherNo.Contains(context)) ).Include(o => o.Outlet).Include(m=>m.OrderOutletLiquors).OrderByDescending(x => x.ModifiedOn);
            var model = new PagedList<Order>(orders, page, pageSize);

            return View(model);
        }

        public IActionResult Detail(Guid? id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            ViewData["OutletID"] = new SelectList(_context.Outlets, "ID", "Name");
            var units = _context.Units.ToList();
            var packunits = _context.PackingUnits.ToList();
            var liquors = _context.Liquors.ToList();
            ViewBag.units = units;
            ViewBag.liquors = liquors;
            ViewBag.packunits = packunits;
            ViewBag.action = "details";
            ViewBag.notFromDB = true;

            if (id == null)
            {
                return NotFound();
            }

            var order = _context.Orders.Where(x => !x.IsDelete && x.ID == id).Include(x => x.OrderOutletLiquors).FirstOrDefault();

            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        public IActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }

            ViewData["OutletID"] = new SelectList(_context.Outlets, "ID", "Name");
            var units = _context.Units.ToList();
            ViewBag.units = units;
            ViewBag.action = "create";
            ViewBag.notFromDB = true;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Order order)
        {
            List<string> overQuantityItems = new List<string>();
            List<string> qtyZeroItems = new List<string>();
            if (order.OrderOutletLiquors != null)
            {
                foreach (var liquor in order.OrderOutletLiquors)
                {
                    if (liquor.Quantity < 1)
                    {
                        var liq = _context.Liquors.Where(x => x.ID == liquor.LiquorID).FirstOrDefault();
                        qtyZeroItems.Add(liq.Code);
                    }
                    else
                    {
                        var outletliquor = _context.OutletLiquors.Where(x => x.LiquorID == liquor.LiquorID && x.OutletID == order.OutletID).FirstOrDefault();
                        if (outletliquor != null)
                        {
                            outletliquor.Quantity -= liquor.QuantityInSmallestUnit;
                            if (outletliquor.Quantity < 0)
                            {
                                var liq = _context.Liquors.Where(x => x.ID == outletliquor.LiquorID).FirstOrDefault();
                                overQuantityItems.Add(liq.Code);
                            }
                        }
                    }
                    
                }
            }
            else
            {
                ViewData["OutletID"] = new SelectList(_context.Outlets, "ID", "Name", order.OutletID);
                ViewData["code"] = order.VoucherNo;
                var o_units = _context.Units.ToList();
                var o_packunits = _context.PackingUnits.ToList();
                var o_liquors = _context.Liquors.ToList();
                ViewBag.units = o_units;
                ViewBag.packunits = o_packunits;
                ViewBag.liquors = o_liquors;
                ViewBag.action = "edit";
                string error = "Please add liquors";
                ViewBag.error = error;
                ViewBag.notFromDB = true;
                return View(order);
            }

            if (overQuantityItems.Count() > 0 || qtyZeroItems.Count()>0)
            {
                ViewData["OutletID"] = new SelectList(_context.Outlets, "ID", "Name", order.OutletID);
                ViewData["code"] = order.VoucherNo;
                var o_units = _context.Units.ToList();
                var o_packunits = _context.PackingUnits.ToList();
                var o_liquors = _context.Liquors.ToList();
                ViewBag.units = o_units;
                ViewBag.packunits = o_packunits;
                ViewBag.liquors = o_liquors;
                ViewBag.action = "edit";
                string error = "";
                if (overQuantityItems.Count() > 0)
                {
                    var i = 0;
                    foreach (var item in overQuantityItems.Distinct())
                    {
                        error += i == 0 ? item : "," + item;
                        i++;
                    }
                    if (error != "")
                    {
                        error = "The Quantity of " + error + " (is)are over limit.";
                    }
                }
                else
                {
                    var i = 0;
                    foreach (var item in qtyZeroItems.Distinct())
                    {
                        error += i == 0 ? item : "," + item;
                        i++;
                    }
                    if (error != "")
                    {
                        error = "The Quantity of " + error + " must be greater than zero.";
                    }
                }
                ViewBag.error = error;
                ViewBag.notFromDB = true;
                return View(order);
            }

            order.ID = Guid.NewGuid();
            order.CreatedBy = _userManager.GetUserId(User);
            order.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            order.VoucherNo = GetVoucherNoByOutlet(order.OutletID, true);
            var outlet = _context.Outlets.Find(order.OutletID);
            outlet.VoucherCounter += 1;
            
            _context.Add(order);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public JsonResult GetLiquorsByOutlet(Guid id, Guid? orderid) // Outlet ID
        {
            List<object> data = new List<object>();
            var orderOutletMedcines = _context.OrderOutletLiquors.Where(x => x.OrderID == orderid).ToList();
            var outletLiquors = _context.OutletLiquors.Include(x => x.Liquor).ThenInclude(x => x.PackingUnits).Where(x => x.OutletID == id && !x.Liquor.IsDelete && (x.Quantity > 0 || orderOutletMedcines.Any(o => o.LiquorID == x.LiquorID))).ToList();
            var packingunits = _context.PackingUnits;
            if (outletLiquors != null)
            {
                foreach (var med in outletLiquors)
                {
                    List<object> punits = new List<object>();
                    med.Liquor.PackingUnits = packingunits.Where(x => x.LiquorID == med.LiquorID).ToList();
                    foreach (var punit in med.Liquor.PackingUnits)
                    {
                        var unitname = _context.Units.Find(punit.UnitID).Description;
                        var unittoadd = new
                        {
                            id = punit.UnitID,
                            name = unitname,
                            unitOrder = punit.UnitOrder,
                            qtyInParent = punit.QuantityInParent,
                            saleamount = punit.SaleAmount,
                            selected = ""
                        };
                        punits.Add(unittoadd);
                    }
                    var obj = new
                    {
                        liquorID = med.LiquorID,
                        liquorCode = med.Liquor.Code,
                        liquorName = med.Liquor.Name,
                        barcode = med.Liquor.Barcode == null ? "":med.Liquor.Barcode,
                        packingUnitID = "",
                        unitPrice = 0,
                        totalQty = med.Quantity,
                        qty = 0,
                        amount = 0,
                        sortorder = 0,
                        qtyInSmallestUnit = 0,
                        units = punits
                    };
                    data.Add(obj);
                }
            }
            return Json(data);
        }

        [HttpGet]
        public string GetVoucherNoByOutlet(Guid id, bool isOT = false) // Outlet ID, isOT = "Is it Operational Theatre type?"
        {
            var voucher = "";
                var outlet = _context.Outlets.Where(x => !x.IsDelete).SingleOrDefault(x => x.ID == id);

                if (outlet != null)
                {
                    voucher = outlet.VoucherPrefix + (outlet.VoucherCounter + 1).ToString("000000");
                } 
            return voucher;
        }
    }
}