﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LiquorProject.Data;
using LiquorProject.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LiquorProject.Controllers
{
    public class LiquorTypeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public LiquorTypeController(UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        // GET: LiquorTypes
        public async Task<IActionResult> Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            return View(await _context.LiquorTypes.Where(x => !x.IsDelete).OrderBy(x => x.Name).ToListAsync());
        }

        // GET: LiquorTypes/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            if (id == null)
            {
                return NotFound();
            }

            var LiquorType = await _context.LiquorTypes.Where(x => !x.IsDelete).OrderBy(x => x.Name)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (LiquorType == null)
            {
                return NotFound();
            }

            return View(LiquorType);
        }

        // GET: LiquorTypes/Create
        public IActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            return View();
        }

        // POST: LiquorTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(LiquorType LiquorType)
        {
            if (ModelState.IsValid)
            {
                LiquorType.ID = Guid.NewGuid();
                LiquorType.CreatedBy = _userManager.GetUserId(User);
                LiquorType.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
                _context.Add(LiquorType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(LiquorType);
        }

        // GET: LiquorTypes/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var LiquorType = await _context.LiquorTypes.Where(x => !x.IsDelete).OrderBy(x => x.Name).SingleOrDefaultAsync(m => m.ID == id);
            if (LiquorType == null)
            {
                return NotFound();
            }
            return View(LiquorType);
        }

        // POST: LiquorTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, LiquorType LiquorType)
        {
            if (id != LiquorType.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    LiquorType.ModifiedBy = _userManager.GetUserId(User);
                    _context.Update(LiquorType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LiquorTypeExists(LiquorType.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(LiquorType);
        }

        // GET: LiquorTypes/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var LiquorType = await _context.LiquorTypes.Where(x => !x.IsDelete).OrderBy(x => x.Name)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (LiquorType == null)
            {
                return NotFound();
            }

            return View(LiquorType);
        }

        // POST: LiquorTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var LiquorType = await _context.LiquorTypes.Where(x => !x.IsDelete).OrderBy(x => x.Name).SingleOrDefaultAsync(m => m.ID == id);
            LiquorType.IsDelete = true;
            //_context.LiquorTypes.Remove(LiquorType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LiquorTypeExists(Guid id)
        {
            return _context.LiquorTypes.Where(x => !x.IsDelete).OrderBy(x => x.Name).Any(e => e.ID == id);
        }
    }
}