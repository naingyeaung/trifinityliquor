﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LiquorProject.Data;
using LiquorProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PagedList.Core;

namespace LiquorProject.Controllers
{
    public class OutletLiquorController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly int _page;
        private readonly int _pageSize;

        public OutletLiquorController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, Pagination pagination)
        {
            _userManager = userManager;
            _context = context;
            _page = pagination.Page;
            _pageSize = pagination.PageSize;
        }

        // GET: OutletLiquors
        public ActionResult Index(string context = "", int page = 0, int pageSize = 0)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            page = page == 0 ? this._page : page;
            pageSize = pageSize == 0 ? this._pageSize : pageSize;
            var applicationDbContext = _context.OutletLiquors.Where(x => !x.Liquor.IsDelete && (string.IsNullOrEmpty(context) ? true : (x.Liquor.Code.Contains(context) || x.Liquor.Name.Contains(context)))).OrderByDescending(x => x.Outlet.Name).ThenBy(x => x.Liquor.Code).Include(m => m.Outlet).Include(m => m.Liquor).ThenInclude(x => x.PackingUnits);
            ViewBag.units = _context.Units.Where(u => applicationDbContext.Any(x => x.Liquor.PackingUnits.Any(y => y.UnitID == u.ID))).ToList();
            var model = new PagedList<OutletLiquor>(applicationDbContext, page, pageSize);

            return View(model);
        }

        public IActionResult ExportOutletStock()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }

            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            string[] headers = { "No", "Code", "Name", "Qty" };

            var liquors = _context.OutletLiquors.Where(x => !x.Liquor.IsDelete).OrderByDescending(x => x.Outlet.Name).ThenBy(x => x.Liquor.Code).Include(m => m.Outlet).Include(m => m.Liquor).ThenInclude(x => x.PackingUnits);
            List<StockLiquorExcel> datas = new List<StockLiquorExcel>();

            int count = 0;
            foreach(var liquor in liquors)
            {
                count++;
                foreach(var pcku in liquor.Liquor.PackingUnits)
                {
                    pcku.Unit = _context.Units.Where(x => x.ID == pcku.UnitID).FirstOrDefault();
                }
                StockLiquorExcel data = new StockLiquorExcel();
                data.No = count.ToString();
                data.Code = liquor.Liquor.Code;
                data.Name = liquor.Liquor.Name;
                data.Qty = Common.ChangeQtySetToString(Common.ChangeMinQtyToQtySet(liquor.Liquor, liquor.Quantity));
                datas.Add(data);
            }

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
            for (int cell = 0; cell < headers.Length; cell++)
            {
                workSheet.Cells[1, cell + 1].Value = headers[cell];
            }
            workSheet.Cells[2, 1].LoadFromCollection(datas, false);
            var headerCells = workSheet.Cells["A1:D1"];
            headerCells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            headerCells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            for (int cell = 0; cell < headers.Length; cell++)
            {
                workSheet.Column(cell + 1).AutoFit();
            }
            headerCells.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            headerCells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Purple);
            headerCells.Style.Font.Color.SetColor(System.Drawing.Color.White);

            string excelName = "OutletStockList.xlsx";
            byte[] result;

            result = excel.GetAsByteArray();

            return File(result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
        }

        // GET: OutletLiquors/Details/5
        public ActionResult Details(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            return View();
        }

        // GET: OutletLiquors/Create
        public ActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            return View();
        }

        // POST: OutletLiquors/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: OutletLiquors/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: OutletLiquors/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: OutletLiquors/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: OutletLiquors/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}