﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LiquorProject.Data;
using LiquorProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LiquorProject.Controllers
{
    [Authorize(Roles = "Superadmin, Admin")]
    public class UnitController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public UnitController(UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        // GET: Units
        public async Task<IActionResult> Index()
        {
            return View(await _context.Units.Where(x => !x.IsDelete).OrderBy(x => x.UnitOrder).ThenBy(x => x.Description).ToListAsync());
        }

        // GET: Units/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var unit = await _context.Units.Where(x => !x.IsDelete).OrderBy(x => x.UnitOrder).ThenBy(x => x.Description)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (unit == null)
            {
                return NotFound();
            }

            return View(unit);
        }

        [Authorize(Roles = "Superadmin")]
        // GET: Units/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Units/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Superadmin")]
        public async Task<IActionResult> Create(Unit unit)
        {
            if (ModelState.IsValid)
            {
                unit.ID = Guid.NewGuid();
                unit.CreatedBy = _userManager.GetUserId(User);
                unit.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
                _context.Add(unit);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(unit);
        }

        [Authorize(Roles = "Superadmin")]
        // GET: Units/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var unit = await _context.Units.Where(x => !x.IsDelete).OrderBy(x => x.UnitOrder).ThenBy(x => x.Description).SingleOrDefaultAsync(m => m.ID == id);
            if (unit == null)
            {
                return NotFound();
            }
            return View(unit);
        }

        // POST: Units/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Superadmin")]
        public async Task<IActionResult> Edit(Guid id, Unit unit)
        {
            if (id != unit.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    unit.ModifiedBy = _userManager.GetUserId(User);
                    _context.Update(unit);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UnitExists(unit.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(unit);
        }

        // GET: Units/Delete/5
        [Authorize(Roles = "Superadmin")]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var unit = await _context.Units.Where(x => !x.IsDelete).OrderBy(x => x.UnitOrder).ThenBy(x => x.Description)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (unit == null)
            {
                return NotFound();
            }

            return View(unit);
        }

        // POST: Units/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Superadmin")]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var unit = await _context.Units.Where(x => !x.IsDelete).OrderBy(x => x.UnitOrder).ThenBy(x => x.Description).SingleOrDefaultAsync(m => m.ID == id);
            unit.IsDelete = true;
            //_context.Units.Remove(unit);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UnitExists(Guid id)
        {
            return _context.Units.Where(x => !x.IsDelete).OrderBy(x => x.UnitOrder).ThenBy(x => x.Description).Any(e => e.ID == id);
        }
    }
}