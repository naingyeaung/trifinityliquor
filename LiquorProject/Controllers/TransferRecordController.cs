﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LiquorProject.Data;
using LiquorProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PagedList.Core;

namespace LiquorProject.Controllers
{
    public class TransferRecordController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly int _page;
        private readonly int _pageSize;

        public TransferRecordController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, Pagination pagination)
        {
            _userManager = userManager;
            _context = context;
            _page = pagination.Page;
            _pageSize = pagination.PageSize;
        }

        // GET: TransferRecords
        public ActionResult Index(string context = "", int page = 0, int pageSize = 0)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            page = page == 0 ? this._page : page;
            pageSize = pageSize == 0 ? this._pageSize : pageSize;
            var applicationDbContext = _context.TransferRecords.Include(t => t.MainStore).Include(t => t.Outlet).Include(x => x.TransferLiquors).OrderByDescending(x => x.ModifiedOn);
            ViewBag.liquors = _context.Liquors.Where(m => applicationDbContext.Any(x => x.TransferLiquors.Any(y => y.LiquorID == m.ID))).ToList();
            var model = new PagedList<TransferRecord>(applicationDbContext, page, pageSize);

            return View(model);
        }

        public ActionResult ExportTransferRecord()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            List<TransferRecordExcelModel> records = new List<TransferRecordExcelModel>();
            var applicationDbContext = _context.TransferRecords.Include(t => t.MainStore).Include(t => t.Outlet).Include(x => x.TransferLiquors).OrderByDescending(x => x.ModifiedOn);
            var i = 0;
            foreach(var item in applicationDbContext)
            {
                i++;
                TransferRecordExcelModel record = new TransferRecordExcelModel();
                record.No = i.ToString();
                record.TransferDate = item.TransferDate.ToString("MM-dd-yyyy");
                var liquors = "";
                var liqCount = 0;
                foreach (var liquor in item.TransferLiquors)
                {
                    liquor.Liquor = _context.Liquors.Find(liquor.LiquorID);
                    liquors += liquor.Liquor != null ? (liqCount == 0 ? liquor.Liquor.Name : ", " + liquor.Liquor.Name) : "";

                    if (liquor.Liquor != null)
                    {
                        liqCount++;
                    }
                }
                record.Liquors = liquors;
                record.ItemCount = item.TransferLiquors.Count().ToString();
                record.TotalQty = item.TransferLiquors.Sum(x => x.Quantity).ToString();

                records.Add(record);

            }

            string[] headers = { "No", "TransferDate", "Liquors", "ItemCount", "TotalQty" };

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
            for (int cell = 0; cell < headers.Length; cell++)
            {
                workSheet.Cells[1, cell + 1].Value = headers[cell];
            }
            workSheet.Cells[2, 1].LoadFromCollection(records, false);
            var headerCells = workSheet.Cells["A1:E1"];
            headerCells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            headerCells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            for (int cell = 0; cell < headers.Length; cell++)
            {
                workSheet.Column(cell + 1).AutoFit();
            }
            headerCells.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            headerCells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Purple);
            headerCells.Style.Font.Color.SetColor(System.Drawing.Color.White);

            string excelName = "TransferRecord.xlsx";
            byte[] result;

            result = excel.GetAsByteArray();

            return File(result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
        }

        // GET: TransferRecords/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            if (id == null)
            {
                return NotFound();
            }

            var transferRecord = await _context.TransferRecords
                .Include(t => t.MainStore)
                .Include(t => t.Outlet)
                .Include(t => t.TransferLiquors)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (transferRecord == null)
            {
                return NotFound();
            }

            ViewData["MainStoreID"] = new SelectList(_context.MainStores, "ID", "Name", transferRecord.MainStoreID);
            ViewData["OutletID"] = new SelectList(_context.Outlets, "ID", "Name", transferRecord.OutletID);
            var Liquors = _context.MainStoreLiquors.Include(x => x.Liquor).ThenInclude(x => x.PackingUnits).OrderBy(x => x.Liquor.Code).ToList();
            var units = _context.Units.ToList();
            ViewBag.liquors = Liquors;
            ViewBag.units = units;

            return View(transferRecord);
        }

        // GET: TransferRecords/Create
        public IActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            ViewData["MainStoreID"] = new SelectList(_context.MainStores, "ID", "Name");
            ViewData["OutletID"] = new SelectList(_context.Outlets, "ID", "Name");
            var Liquors = _context.MainStoreLiquors.Where(x => !x.Liquor.IsDelete).Include(x => x.Liquor).ThenInclude(x => x.PackingUnits).Where(x => !x.Liquor.IsDelete && x.Quantity > 0).OrderBy(x => x.Liquor.Code).ToList();
            var units = _context.Units.ToList();
            ViewBag.liquors = Liquors;
            ViewBag.units = units;
            return View();
        }

        // POST: TransferRecords/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TransferRecord transferRecord)
        {
            if (ModelState.IsValid)
            {
                List<string> overQuantityItems = new List<string>();
                foreach (var tmed in transferRecord.TransferLiquors)
                {
                    var outletLiquor = _context.OutletLiquors.Where(x => x.LiquorID == tmed.LiquorID && x.OutletID == transferRecord.OutletID).SingleOrDefault();

                    var mainStoreLiquor = _context.MainStoreLiquors.Where(x => x.LiquorID == tmed.LiquorID && x.MainStoreID == transferRecord.MainStoreID).Include(x => x.Liquor).SingleOrDefault();
                    if (mainStoreLiquor != null)
                    {
                        mainStoreLiquor.Quantity -= tmed.QuantityInSmallestUnit;
                        if (mainStoreLiquor.Quantity < 0)
                        {
                            overQuantityItems.Add(mainStoreLiquor.Liquor.Code);
                        }
                    }

                    if (outletLiquor != null)
                    {
                        outletLiquor.Quantity += tmed.QuantityInSmallestUnit;
                    }
                    else
                    {
                        OutletLiquor newOutletLiquor = new OutletLiquor();
                        newOutletLiquor.ID = Guid.NewGuid();
                        newOutletLiquor.OutletID = transferRecord.OutletID;
                        newOutletLiquor.LiquorID = tmed.LiquorID;
                        newOutletLiquor.Quantity = tmed.QuantityInSmallestUnit;
                        _context.OutletLiquors.Add(newOutletLiquor);
                    }
                }
                if (overQuantityItems.Count() > 0)
                {
                    string error = "";
                    var i = 0;
                    foreach (var item in overQuantityItems.Distinct())
                    {
                        error += i == 0 ? item : "," + item;
                        i++;
                    }
                    if (error != "")
                    {
                        error = "The Quantity of " + error + " (is)are over limit.";
                    }
                    ViewBag.error = error;
                    ViewData["MainStoreID"] = new SelectList(_context.MainStores, "ID", "Name", transferRecord.MainStoreID);
                    ViewData["OutletID"] = new SelectList(_context.Outlets, "ID", "Name", transferRecord.OutletID);
                    var ms_Liquors = _context.MainStoreLiquors.Include(x => x.Liquor).ThenInclude(x => x.PackingUnits).Where(x => !x.Liquor.IsDelete && (x.Quantity > 0 || transferRecord.TransferLiquors.Any(o => o.LiquorID == x.LiquorID))).OrderBy(x => x.Liquor.Code).ToList();
                    var ms_units = _context.Units.ToList();
                    ViewBag.Liquors = ms_Liquors;
                    ViewBag.units = ms_units;
                    return View(transferRecord);
                }

                transferRecord.ID = Guid.NewGuid();
                transferRecord.CreatedBy = _userManager.GetUserId(User);
                transferRecord.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
                _context.Add(transferRecord);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MainStoreID"] = new SelectList(_context.MainStores, "ID", "Name", transferRecord.MainStoreID);
            ViewData["OutletID"] = new SelectList(_context.Outlets, "ID", "Name", transferRecord.OutletID);
            var Liquors = _context.MainStoreLiquors.Where(x => !x.Liquor.IsDelete).Include(x => x.Liquor).ThenInclude(x => x.PackingUnits).Where(x => !x.Liquor.IsDelete && x.Quantity > 0).OrderBy(x => x.Liquor.Code).ToList();
            var units = _context.Units.ToList();
            ViewBag.Liquors = Liquors;
            ViewBag.units = units;
            return View(transferRecord);
        }

        // GET: TransferRecords/Edit/5
        //public async Task<IActionResult> Edit(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var transferRecord = await _context.TransferRecords.Include(t => t.TransferLiquors).SingleOrDefaultAsync(m => m.ID == id);
        //    if (transferRecord == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["MainStoreID"] = new SelectList(_context.MainStores, "ID", "Name", transferRecord.MainStoreID);
        //    ViewData["OutletID"] = new SelectList(_context.Outlets, "ID", "Name", transferRecord.OutletID);
        //    var Liquors = _context.MainStoreLiquors.Include(x => x.Liquor).ThenInclude(x => x.PackingUnits).OrderBy(x => x.Liquor.Code).ToList();
        //    var units = _context.Units.ToList();
        //    ViewBag.Liquors = Liquors;
        //    ViewBag.units = units;
        //    return View(transferRecord);
        //}

        //// POST: TransferRecords/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(Guid id, TransferRecord transferRecord)
        //{
        //    if (id != transferRecord.ID)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(transferRecord);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!TransferRecordExists(transferRecord.ID))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["MainStoreID"] = new SelectList(_context.MainStores, "ID", "Name", transferRecord.MainStoreID);
        //    ViewData["OutletID"] = new SelectList(_context.Outlets, "ID", "Name", transferRecord.OutletID);
        //    var Liquors = _context.MainStoreLiquors.Include(x => x.Liquor).ThenInclude(x => x.PackingUnits).OrderBy(x => x.Liquor.Code).ToList();
        //    var units = _context.Units.ToList();
        //    ViewBag.Liquors = Liquors;

        //    ViewBag.units = units;
        //    return View(transferRecord);
        //}

        // GET: TransferRecords/Delete/5
        [Authorize(Roles = "Superadmin, Admin")]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transferRecord = await _context.TransferRecords
                .Include(t => t.MainStore)
                .Include(t => t.Outlet)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (transferRecord == null)
            {
                return NotFound();
            }

            return View(transferRecord);
        }

        // POST: TransferRecords/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Superadmin, Admin")]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var transferRecord = await _context.TransferRecords.Include(x => x.TransferLiquors).SingleOrDefaultAsync(m => m.ID == id);
            foreach (var tmed in transferRecord.TransferLiquors)
            {
                var outletLiquor = _context.OutletLiquors.Where(x => x.LiquorID == tmed.LiquorID && x.OutletID == transferRecord.OutletID).SingleOrDefault();

                var mainStoreLiquor = _context.MainStoreLiquors.Where(x => x.LiquorID == tmed.LiquorID && x.MainStoreID == transferRecord.MainStoreID).SingleOrDefault();
                if (mainStoreLiquor != null)
                {
                    mainStoreLiquor.Quantity += tmed.QuantityInSmallestUnit;
                }

                if (outletLiquor != null)
                {
                    outletLiquor.Quantity -= tmed.QuantityInSmallestUnit;
                }
            }
            _context.TransferLiquors.RemoveRange(transferRecord.TransferLiquors);
            _context.TransferRecords.Remove(transferRecord);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TransferRecordExists(Guid id)
        {
            return _context.TransferRecords.Any(e => e.ID == id);
        }
    }
}