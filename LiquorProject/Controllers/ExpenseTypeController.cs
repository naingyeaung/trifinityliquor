﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LiquorProject.Data;
using LiquorProject.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PagedList.Core;

namespace LiquorProject.Controllers
{
    public class ExpenseTypeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly int _page;
        private readonly int _pageSize;

        public ExpenseTypeController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, Pagination pagination)
        {
            _userManager = userManager;
            _context = context;
            _page = pagination.Page;
            _pageSize = pagination.PageSize;
        }
        public IActionResult Index(int page = 0, int pageSize = 0)
        {
            var types = _context.ExpenseTypes.Where(x => !x.IsDelete);
            page = page == 0 ? this._page : page;
            pageSize = pageSize == 0 ? this._pageSize : pageSize;

            var model = new PagedList<ExpenseType>(types, page, pageSize);

            return View(model);
        }

        public IActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ExpenseType model)
        {
            model.ID = Guid.NewGuid();
            model.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            model.ModifiedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            model.CreatedBy = _userManager.GetUserId(User);
            model.ModifiedBy = _userManager.GetUserId(User);
            model.IsDelete = false;
            _context.ExpenseTypes.Add(model);

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Details(Guid id)
        {
            var expenseType = _context.ExpenseTypes.Where(x=>!x.IsDelete && x.ID==id).FirstOrDefault();
            if (expenseType != null)
            {
                return View(expenseType);
            }
            return NotFound();
        }

        public IActionResult Delete(Guid id)
        {
            var expenseType = _context.ExpenseTypes.Where(x=>!x.IsDelete && x.ID==id).FirstOrDefault();
            if (expenseType != null)
            {
                return View(expenseType);
            }

            return NotFound();
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            var expenseType = _context.ExpenseTypes.Where(x => !x.IsDelete && x.ID == id).FirstOrDefault();

            expenseType.IsDelete = true;
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}