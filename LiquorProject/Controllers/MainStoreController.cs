﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LiquorProject.Data;
using LiquorProject.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LiquorProject.Controllers
{
    public class MainStoreController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public MainStoreController(UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        // GET: MainStores
        public async Task<IActionResult> Index()
        {
            return View(await _context.MainStores.Where(x => !x.IsDelete).OrderByDescending(x => x.ModifiedOn).ToListAsync());
        }

        // GET: MainStores/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mainStore = await _context.MainStores.Where(x => !x.IsDelete)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (mainStore == null)
            {
                return NotFound();
            }

            return View(mainStore);
        }

        // GET: MainStores/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MainStores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MainStore mainStore)
        {
            if (ModelState.IsValid)
            {
                mainStore.ID = Guid.NewGuid();
                mainStore.CreatedBy = _userManager.GetUserId(User);
                mainStore.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
                _context.Add(mainStore);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(mainStore);
        }

        // GET: MainStores/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mainStore = await _context.MainStores.Where(x => !x.IsDelete).SingleOrDefaultAsync(m => m.ID == id);
            if (mainStore == null)
            {
                return NotFound();
            }
            return View(mainStore);
        }

        // POST: MainStores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, MainStore mainStore)
        {
            if (id != mainStore.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(mainStore);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MainStoreExists(mainStore.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(mainStore);
        }

        // GET: MainStores/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mainStore = await _context.MainStores.Where(x => !x.IsDelete)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (mainStore == null)
            {
                return NotFound();
            }

            return View(mainStore);
        }

        // POST: MainStores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var mainStore = await _context.MainStores.Where(x => !x.IsDelete).SingleOrDefaultAsync(m => m.ID == id);
            mainStore.IsDelete = true;
            //_context.MainStores.Remove(mainStore);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MainStoreExists(Guid id)
        {
            return _context.MainStores.Where(x => !x.IsDelete).Any(e => e.ID == id);
        }
    }
}