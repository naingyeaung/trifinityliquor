﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LiquorProject.Data;
using LiquorProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PagedList.Core;
using PagedList.Core.Mvc;

namespace LiquorProject.Controllers
{
    public class LiquorController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly int _page;
        private readonly int _pageSize;

        public LiquorController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, Pagination pagination)
        {
            _userManager = userManager;
            _context = context;
            _page = pagination.Page;
            _pageSize = pagination.PageSize;
        }

        // GET: Liquors
        public IActionResult Index(string context = "", int page = 0, int pageSize = 0)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            page = page == 0 ? this._page : page;
            pageSize = pageSize == 0 ? this._pageSize : pageSize;
            var applicationDbContext = _context.Liquors.Where(x => !x.IsDelete && (string.IsNullOrEmpty(context) ? true : (x.Code.Contains(context) || x.Name.Contains(context)))).Include(m => m.LiquorType).Include(m => m.PackingUnits).OrderByDescending(x => x.ModifiedOn);
            var model = new PagedList<Liquor>(applicationDbContext, page, pageSize);

            return View(model);
        }

        public IActionResult ExportLiquor()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }

            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            string[] headers = { "No", "Name", "Code", "Price"};

            var liquors = _context.Liquors.Where(x => !x.IsDelete).OrderBy(x => x.ModifiedOn);

            List<LiquorExcel> datas = new List<LiquorExcel>();
            var count = 0;
            foreach(var liquor in liquors)
            {
                count++;
                LiquorExcel data = new LiquorExcel();
                data.No = count.ToString();
                data.Name = liquor.Name;
                data.Code = liquor.Code;
                data.Price = liquor.UnitPrice.ToString();
                datas.Add(data);
            }
            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
            for (int cell = 0; cell < headers.Length; cell++)
            {
                workSheet.Cells[1, cell + 1].Value = headers[cell];
            }
            workSheet.Cells[2, 1].LoadFromCollection(datas, false);
            var headerCells = workSheet.Cells["A1:D1"];
            headerCells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            headerCells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            for (int cell = 0; cell < headers.Length; cell++)
            {
                workSheet.Column(cell + 1).AutoFit();
            }
            headerCells.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            headerCells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Purple);
            headerCells.Style.Font.Color.SetColor(System.Drawing.Color.White);

            string excelName = "LiquorList.xlsx";
            byte[] result;

            result = excel.GetAsByteArray();

            return File(result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);

        }

        // GET: Liquors/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            if (id == null)
            {
                return NotFound();
            }

            var Liquor = await _context.Liquors.Where(x => !x.IsDelete).OrderByDescending(x => x.ModifiedOn)
                .Include(x => x.LiquorType).Include(x => x.PackingUnits)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (Liquor == null)
            {
                return NotFound();
            }
            ViewData["LiquorTypeID"] = new SelectList(_context.LiquorTypes.Where(x => !x.IsDelete), "ID", "Name", Liquor.LiquorTypeID);
            var units = _context.Units.Where(x => !x.IsDelete).OrderBy(x => x.UnitOrder).ThenBy(x => x.Description).ToList();
            ViewBag.units = units;
            ViewData["code"] = Liquor.Code;
            return View(Liquor);
        }

        // GET: Liquors/Create
        public IActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            ViewData["LiquorTypeID"] = new SelectList(_context.LiquorTypes.Where(x => !x.IsDelete), "ID", "Name");
            var units = _context.Units.Where(x => !x.IsDelete).OrderBy(x => x.UnitOrder).ThenBy(x => x.Description).ToList();
            ViewBag.units = units;
            return View();
        }

        // POST: Liquors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Liquor Liquor)
        {
            if (ModelState.IsValid)
            {
                var Liquorcounter = _context.Counters.Where(x => x.CounterType == 1).First();
                var Liquorcount = Liquorcounter.Count + 1;
                Liquorcounter.Count = Liquorcount;
                var code = _context.LiquorTypes.Where(x => !x.IsDelete && x.ID == Liquor.LiquorTypeID).First().Prefix + Liquorcount.ToString("000000");

                Liquor.Code = code;
                Liquor.CreatedBy = _userManager.GetUserId(User);
                Liquor.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
                _context.Add(Liquor);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["LiquorTypeID"] = new SelectList(_context.LiquorTypes.Where(x => !x.IsDelete), "ID", "Name", Liquor.LiquorTypeID);
            var units = _context.Units.Where(x => !x.IsDelete).OrderBy(x => x.UnitOrder).ThenBy(x => x.Description).ToList();
            ViewBag.units = units;
            return View(Liquor);
        }

        //[Authorize(Roles = "Superadmin, Admin")]
        // GET: Liquors/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            if (id == null)
            {
                return NotFound();
            }

            var Liquor = await _context.Liquors.Where(x => !x.IsDelete).OrderByDescending(x => x.ModifiedOn).Include(x => x.LiquorType).Include(x => x.PackingUnits).SingleOrDefaultAsync(m => m.ID == id);
            if (Liquor == null)
            {
                return NotFound();
            }
            ViewData["LiquorTypeID"] = new SelectList(_context.LiquorTypes.Where(x => !x.IsDelete), "ID", "Name", Liquor.LiquorTypeID);
            var units = _context.Units.Where(x => !x.IsDelete).OrderBy(x => x.UnitOrder).ThenBy(x => x.Description).ToList();
            ViewBag.units = units;
            ViewData["code"] = Liquor.Code;
            return View(Liquor);
        }

        //POST: Liquors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Authorize(Roles = "Superadmin, Admin")]
        public async Task<IActionResult> Edit(Guid id, Liquor Liquor)
        {
            if (id != Liquor.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.PackingUnits.RemoveRange(_context.PackingUnits.Where(x => x.LiquorID == Liquor.ID));
                    Liquor.ModifiedBy = _userManager.GetUserId(User);
                    _context.Update(Liquor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LiquorExists(Liquor.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["LiquorTypeID"] = new SelectList(_context.LiquorTypes.Where(x => !x.IsDelete), "ID", "Name", Liquor.LiquorTypeID);
            var units = _context.Units.Where(x => !x.IsDelete).OrderBy(x => x.UnitOrder).ThenBy(x => x.Description).ToList();
            ViewBag.units = units;
            ViewData["code"] = Liquor.Code;
            return View(Liquor);
        }

        //[Authorize(Roles = "Superadmin, Admin")]
        // GET: Liquors/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            if (id == null)
            {
                return NotFound();
            }

            var Liquor = await _context.Liquors.Where(x => !x.IsDelete).OrderByDescending(x => x.ModifiedOn)
                .Include(x => x.LiquorType).Include(x => x.PackingUnits)
                .SingleOrDefaultAsync(m => m.ID == id);
            ViewData["code"] = Liquor.Code;
            if (Liquor == null)
            {
                return NotFound();
            }

            return View(Liquor);
        }

        // POST: Liquors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        //[Authorize(Roles = "Superadmin, Admin")]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            var Liquor = await _context.Liquors.Where(x => !x.IsDelete).OrderByDescending(x => x.ModifiedOn).SingleOrDefaultAsync(m => m.ID == id);
            //_context.PackingUnits.RemoveRange(_context.PackingUnits.Where(x => x.LiquorID == id));
            //_context.MainStoreLiquors.RemoveRange(_context.MainStoreLiquors.Where(x => x.LiquorID == id));
            //_context.OutletLiquors.RemoveRange(_context.OutletLiquors.Where(x => x.LiquorID == id));
            Liquor.IsDelete = true;
            //_context.Liquors.Remove(Liquor);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LiquorExists(Guid id)
        {
            return _context.Liquors.Where(x => !x.IsDelete).OrderByDescending(x => x.ModifiedOn).Any(e => e.ID == id);
        }
    }
}