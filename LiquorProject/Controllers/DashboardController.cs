﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LiquorProject.Data;
using LiquorProject.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace LiquorProject.Controllers
{
    public class DashboardController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly int _page;
        private readonly int _pageSize;
        public readonly IConfiguration _configuration;

        public DashboardController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, Pagination pagination, IConfiguration configuration)
        {
            _userManager = userManager;
            _context = context;
            _page = pagination.Page;
            _pageSize = pagination.PageSize;
            _configuration = configuration;
        }

        #region daily
        public List<LiquorSale> GetDailySale(DateTime date)
        {
            var orders = _context.Orders.Where(x => (x.CreatedOn ?? DateTime.UtcNow.AddHours(6).AddMinutes(30)).Date == date.Date);
            List<OrderOutletLiquor> orderLiquors = new List<OrderOutletLiquor>();
            List<LiquorSale> liquorSales = new List<LiquorSale>();

            foreach (var order in orders)
            {
                var liquors = _context.OrderOutletLiquors.Where(x => x.OrderID == order.ID);
                orderLiquors.AddRange(liquors);
            }

            var groups = orderLiquors.GroupBy(x => x.LiquorID);
            
            foreach (var liquor in groups)
            {
                Liquor liq = _context.Liquors.Include(x=>x.PackingUnits).Where(x=>x.ID==liquor.FirstOrDefault().LiquorID).FirstOrDefault();
                foreach(var pack in liq.PackingUnits)
                {
                    pack.Unit = _context.Units.Where(x => x.ID == pack.UnitID).FirstOrDefault();
                }
                var totalCount = "";
                var totalCost = "";
                var liquorsGroupByUnit = liquor.GroupBy(x => x.UnitID);
                foreach(var liqbyUnit in liquorsGroupByUnit)
                {
                    var unitName = _context.Units.Where(x => x.ID == liqbyUnit.FirstOrDefault().UnitID).FirstOrDefault().Description;
                    var qty = liqbyUnit.Sum(x => x.Quantity);
                    totalCount += totalCount == "" ? qty + unitName : ", " + qty + unitName;
                    //totalCount += qty + unitName + ", ";
                    totalCost += totalCost == "" ? Math.Round(qty * liqbyUnit.FirstOrDefault().UnitPrice) + "(" + unitName + ")" : ", " + Math.Round(qty * liqbyUnit.FirstOrDefault().UnitPrice) + "(" + unitName + ")";
                    
                }
                LiquorSale sale = new LiquorSale();
                sale.Cost = Math.Round(liquor.Sum(x => x.Amount));
                sale.Count = totalCount;
                sale.Name = _context.Liquors.Where(x => x.ID == liquor.FirstOrDefault().LiquorID).FirstOrDefault().Name;
                sale.CostInUnit = totalCost;

                liquorSales.Add(sale);
            }
            var totalAllCost = "";
            var unitGroups = orderLiquors.GroupBy(x => x.UnitID);
            foreach(var gp in unitGroups)
            {
                var unitName = _context.Units.Where(x => x.ID == gp.FirstOrDefault().UnitID).FirstOrDefault().Description;
                var gpInUnit = gp.GroupBy(x => x.LiquorID);
                var liqCost = 0.0m;
                foreach(var ls in gpInUnit)
                {
                    foreach(var l in ls)
                    {
                        liqCost += (l.Quantity * l.UnitPrice);
                    }
                }
                totalAllCost += totalAllCost == "" ? Math.Round(liqCost) + "(" +unitName + ")" : ", " + Math.Round(liqCost) + "(" + unitName + ")";
            }
            LiquorSale liqForTotal = new LiquorSale();
            liqForTotal.CostInUnit = totalAllCost;
            liqForTotal.Cost = Math.Round(liquorSales.Sum(x => x.Cost));
            liquorSales.Add(liqForTotal);

            return liquorSales;
        }
        public IActionResult DailySale(DateTime date)
        {
            var liquorSales = GetDailySale(date);

            var totalAmount = 0.0m;

            totalAmount = liquorSales.Sum(x => x.Cost);

            ViewBag.TotalAmount = totalAmount;

            return View(liquorSales);
        }

        public IActionResult DailyExpense(DateTime date)
        {
            var expenses = _context.Expenses.Where(x => x.ExpenseDate.Date == date.Date);
            ViewBag.TotalExpense = expenses.Sum(x => x.Cost);

            return View(expenses.ToList());

        }

        public IActionResult DailyRevenue(DateTime date)
        {
            var liquorSales = GetDailySale(date);

            var totalAmount = 0.0m;
            var totalExpense = 0;

            totalExpense = _context.Expenses.Where(x => !x.IsDelete && x.ExpenseDate.Date == date.Date).Sum(x => x.Cost);
            totalAmount = liquorSales.Sum(x => x.Cost);

            ViewBag.TotalAmount = totalAmount;
            ViewBag.TotalExpense = totalExpense;
            ViewBag.TotalRevenue = totalAmount - totalExpense;
            return View(liquorSales);
        }

        public IActionResult ExportDailySale()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }

            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            string[] headers = { "No", "Name", "TotalQty", "TotalCost","TotalCostInUnit" };

            var liquorSales = GetDailySale(DateTime.UtcNow.AddHours(6).AddMinutes(30));

            LiquorSale liqSale = new LiquorSale();

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
            for (int cell = 0; cell < headers.Length; cell++)
            {
                workSheet.Cells[1, cell + 1].Value = headers[cell];
            }
            workSheet.Cells[2, 1].LoadFromCollection(liquorSales, false);
            var headerCells = workSheet.Cells["A1:E1"];
            headerCells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            headerCells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            for (int cell = 0; cell < headers.Length; cell++)
            {
                workSheet.Column(cell + 1).AutoFit();
            }
            headerCells.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            headerCells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Purple);
            headerCells.Style.Font.Color.SetColor(System.Drawing.Color.White);

            string excelName = "DailySale.xlsx";
            byte[] result;

            result = excel.GetAsByteArray();

            return File(result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
        }

        #endregion

        #region monthly

        public List<LiquorSale> GetMonthlySale(DateTime date)
        {
            var orders = _context.Orders.Where(x => (x.CreatedOn ?? DateTime.UtcNow.AddHours(6).AddMinutes(30)).Month == date.Month && (x.CreatedOn ?? DateTime.UtcNow.AddHours(6).AddMinutes(30)).Year == date.Year);
            List<OrderOutletLiquor> orderLiquors = new List<OrderOutletLiquor>();
            List<LiquorSale> liquorSales = new List<LiquorSale>();

            

            foreach (var order in orders)
            {
                var liquors = _context.OrderOutletLiquors.Where(x => x.OrderID == order.ID);
                orderLiquors.AddRange(liquors);
            }

            var groups = orderLiquors.GroupBy(x => x.LiquorID);

            foreach (var liquor in groups)
            {
                Liquor liq = _context.Liquors.Include(x=>x.PackingUnits).Where(x=>x.ID==liquor.FirstOrDefault().LiquorID).FirstOrDefault();
                foreach (var pack in liq.PackingUnits)
                {
                    pack.Unit = _context.Units.Where(x => x.ID == pack.UnitID).FirstOrDefault();
                }
                var totalCount = "";
                var totalCost = "";
                var liquorsGroupByUnit = liquor.GroupBy(x => x.UnitID);
                foreach (var liqbyUnit in liquorsGroupByUnit)
                {
                    var unitName = _context.Units.Where(x => x.ID == liqbyUnit.FirstOrDefault().UnitID).FirstOrDefault().Description;
                    var qty = liqbyUnit.Sum(x => x.Quantity);
                    totalCount += totalCount == "" ? qty + unitName : ", " + qty + unitName;
                    totalCost += totalCost == "" ? Math.Round(qty * liqbyUnit.FirstOrDefault().UnitPrice) + "("+ unitName + ")" : ", " + Math.Round(qty * liqbyUnit.FirstOrDefault().UnitPrice) + "(" + unitName + ")";                 
                    //totalCount += qty + unitName + ", ";
                }
                LiquorSale sale = new LiquorSale();
                sale.Cost = Math.Round(liquor.Sum(x => x.Amount));
                sale.Count = totalCount;
                sale.Name = _context.Liquors.Where(x => x.ID == liquor.FirstOrDefault().LiquorID).FirstOrDefault().Name;
                sale.CostInUnit = totalCost;

                liquorSales.Add(sale);
            }
            var totalAllCost = "";
            var unitGroups = orderLiquors.GroupBy(x => x.UnitID);
            foreach (var gp in unitGroups)
            {
                var unitName = _context.Units.Where(x => x.ID == gp.FirstOrDefault().UnitID).FirstOrDefault().Description;
                var gpInUnit = gp.GroupBy(x => x.LiquorID);
                var liqCost = 0.0m;
                foreach (var ls in gpInUnit)
                {
                    foreach (var l in ls)
                    {
                        liqCost += (l.Quantity * l.UnitPrice);
                    }
                }
                totalAllCost += totalAllCost == "" ? Math.Round(liqCost) + "(" + unitName + ")" : ", " + Math.Round(liqCost) + "(" + unitName + ")";
            }
            LiquorSale liqForTotal = new LiquorSale();
            liqForTotal.CostInUnit = totalAllCost;
            liqForTotal.Cost = Math.Round(liquorSales.Sum(x => x.Cost));
            liquorSales.Add(liqForTotal);

            return liquorSales;
        }

        public IActionResult MonthlySale(DateTime date)
        {
            var liquorSales = GetMonthlySale(date);

            var totalAmount = 0.0m;
            var totalExpense = 0;

            totalAmount = liquorSales.Sum(x => x.Cost);

            ViewBag.TotalAmount = totalAmount;

            return View(liquorSales);
        }

        public IActionResult MonthlyExpense(DateTime date)
        {
            var expenses = _context.Expenses.Where(x => x.ExpenseDate.Month == date.Month && x.ExpenseDate.Year == date.Year);
            ViewBag.TotalExpense = expenses.Sum(x => x.Cost);

            return View(expenses.ToList());
        }

        public IActionResult MonthlyRevenue(DateTime date)
        {
            var liquorSales = GetMonthlySale(date);

            var totalAmount = 0.0m;
            var totalExpense = 0;

            totalExpense = _context.Expenses.Where(x => !x.IsDelete && x.ExpenseDate.Month == date.Month && x.ExpenseDate.Year == date.Year).Sum(x => x.Cost);
            totalAmount = liquorSales.Sum(x => x.Cost);

            ViewBag.TotalAmount = totalAmount;
            ViewBag.TotalExpense = totalExpense;
            ViewBag.TotalRevenue = totalAmount - totalExpense;
            return View(liquorSales);
        }

        public IActionResult ExportMonthlySale()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }

            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            string[] headers = { "No", "Name", "TotalQty", "TotalCost", "TotalCostInUnit" };

            var liquorSales = GetMonthlySale(DateTime.UtcNow.AddHours(6).AddMinutes(30));

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
            for (int cell = 0; cell < headers.Length; cell++)
            {
                workSheet.Cells[1, cell + 1].Value = headers[cell];
            }
            workSheet.Cells[2, 1].LoadFromCollection(liquorSales, false);
            var headerCells = workSheet.Cells["A1:E1"];
            headerCells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            headerCells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            for (int cell = 0; cell < headers.Length; cell++)
            {
                workSheet.Column(cell + 1).AutoFit();
            }
            headerCells.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            headerCells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Purple);
            headerCells.Style.Font.Color.SetColor(System.Drawing.Color.White);

            string excelName = "MonthlySale.xlsx";
            byte[] result;

            result = excel.GetAsByteArray();

            return File(result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
        }

        #endregion
    }
}