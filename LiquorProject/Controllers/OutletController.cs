﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LiquorProject.Data;
using LiquorProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LiquorProject.Controllers
{
    public class OutletController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public OutletController(UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        // GET: Outlets
        public async Task<IActionResult> Index()
        {
            return View(await _context.Outlets.Where(x => !x.IsDelete).OrderByDescending(x => x.ModifiedOn).ToListAsync());
        }

        // GET: Outlets/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var outlet = await _context.Outlets.Where(x => !x.IsDelete).OrderByDescending(x => x.ModifiedOn)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (outlet == null)
            {
                return NotFound();
            }

            return View(outlet);
        }

        // GET: Outlets/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Outlets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Outlet outlet)
        {
            if (ModelState.IsValid)
            {
                outlet.ID = Guid.NewGuid();
                _context.Add(outlet);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(outlet);
        }

        // GET: Outlets/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var outlet = await _context.Outlets.Where(x => !x.IsDelete).OrderByDescending(x => x.ModifiedOn).SingleOrDefaultAsync(m => m.ID == id);
            if (outlet == null)
            {
                return NotFound();
            }
            return View(outlet);
        }

        // POST: Outlets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, Outlet outlet)
        {
            if (id != outlet.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(outlet);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OutletExists(outlet.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(outlet);
        }

        // GET: Outlets/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var outlet = await _context.Outlets.Where(x => !x.IsDelete).OrderByDescending(x => x.ModifiedOn)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (outlet == null)
            {
                return NotFound();
            }

            return View(outlet);
        }

        // POST: Outlets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var outlet = await _context.Outlets.Where(x => !x.IsDelete).OrderByDescending(x => x.ModifiedOn).SingleOrDefaultAsync(m => m.ID == id);
            outlet.IsDelete = true;
            //_context.Outlets.Remove(outlet);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OutletExists(Guid id)
        {
            return _context.Outlets.Where(x => !x.IsDelete).OrderByDescending(x => x.ModifiedOn).Any(e => e.ID == id);
        }
    }
}