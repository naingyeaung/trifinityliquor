﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LiquorProject.Data;
using LiquorProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PagedList.Core;

namespace LiquorProject.Controllers
{
    public class MainStoreStockRecordController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly int _page;
        private readonly int _pageSize;

        public MainStoreStockRecordController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, Pagination pagination)
        {
            _userManager = userManager;
            _context = context;
            _page = pagination.Page;
            _pageSize = pagination.PageSize;
        }

        // GET: MainStoreStockRecords
        public ActionResult Index(string context = "", int page = 0, int pageSize = 0)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            page = page == 0 ? this._page : page;
            pageSize = pageSize == 0 ? this._pageSize : pageSize;
            var applicationDbContext = _context.MainStoreStockRecords.Where(x => string.IsNullOrEmpty(context) ? true : (x.Liquor.Code.Contains(context) || x.Liquor.Name.Contains(context))).OrderByDescending(x => x.StockDate).Include(m => m.MainStore).Include(m => m.Liquor);
            var model = new PagedList<MainStoreStockRecord>(applicationDbContext, page, pageSize);

            return View(model);
        }

        // GET: MainStoreStockRecords/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            if (id == null)
            {
                return NotFound();
            }

            var mainStoreStockRecord = await _context.MainStoreStockRecords.OrderByDescending(x => x.StockDate)
                .Include(m => m.MainStore)
                .Include(m => m.Liquor)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (mainStoreStockRecord == null)
            {
                return NotFound();
            }
            ViewData["MainStoreID"] = new SelectList(_context.MainStores.Where(x => !x.IsDelete), "ID", "Name", mainStoreStockRecord.MainStoreID);
            ViewData["LiquorID"] = new SelectList(_context.Liquors.Where(x => !x.IsDelete).OrderBy(x => x.Code), "ID", "Name", mainStoreStockRecord.LiquorID);
            return View(mainStoreStockRecord);
        }

        // GET: MainStoreStockRecords/Create
        public IActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Redirect("/Identity/Account/Login");
            }
            ViewData["MainStoreID"] = new SelectList(_context.MainStores.Where(x => !x.IsDelete), "ID", "Name");
            ViewData["LiquorID"] = new SelectList(_context.Liquors.Where(x => !x.IsDelete).OrderBy(x => x.Code), "ID", "Name");
            return View();
        }

        // POST: MainStoreStockRecords/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MainStoreStockRecord mainStoreStockRecord)
        {
            if (ModelState.IsValid)
            {
                mainStoreStockRecord.ID = Guid.NewGuid();
                mainStoreStockRecord.CreatedBy = _userManager.GetUserId(User);
                mainStoreStockRecord.CreatedOn = DateTime.UtcNow.AddHours(6).AddMinutes(30);
                var packingUnits = _context.PackingUnits.Where(x => x.LiquorID == mainStoreStockRecord.LiquorID).ToList();
                mainStoreStockRecord.QuantityInSmallestUnit = mainStoreStockRecord.Quantity;
                mainStoreStockRecord.UnitName = "";
                if (packingUnits != null)
                {
                    var unitOrder = packingUnits.Where(x => x.UnitID == mainStoreStockRecord.UnitID).First().UnitOrder;
                    var qty = 1;
                    for (var i = packingUnits.Max(x => x.UnitOrder); i > unitOrder; i--)
                    {
                        qty *= packingUnits[i - 1].QuantityInParent;
                    }
                    mainStoreStockRecord.QuantityInSmallestUnit *= qty;
                    mainStoreStockRecord.UnitName = _context.Units.Where(x => x.ID == mainStoreStockRecord.UnitID).First().Description;
                }
                _context.Add(mainStoreStockRecord);
                await _context.SaveChangesAsync();

                var mainStoreLiquor = _context.MainStoreLiquors.Where(x => x.LiquorID == mainStoreStockRecord.LiquorID && x.MainStoreID == mainStoreStockRecord.MainStoreID).SingleOrDefault();
                if (mainStoreLiquor != null)
                {
                    mainStoreLiquor.Quantity += mainStoreStockRecord.QuantityInSmallestUnit;
                }
                else
                {
                    MainStoreLiquor newMainStoreLiquor = new MainStoreLiquor();
                    newMainStoreLiquor.ID = Guid.NewGuid();
                    newMainStoreLiquor.MainStoreID = mainStoreStockRecord.MainStoreID;
                    newMainStoreLiquor.LiquorID = mainStoreStockRecord.LiquorID;
                    newMainStoreLiquor.Quantity = mainStoreStockRecord.QuantityInSmallestUnit;
                    _context.MainStoreLiquors.Add(newMainStoreLiquor);
                }
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MainStoreID"] = new SelectList(_context.MainStores.Where(x => !x.IsDelete), "ID", "Name", mainStoreStockRecord.MainStoreID);
            ViewData["LiquorID"] = new SelectList(_context.Liquors.Where(x => !x.IsDelete).OrderBy(x => x.Code), "ID", "Name", mainStoreStockRecord.LiquorID);
            return View(mainStoreStockRecord);
        }

        // GET: MainStoreStockRecords/Edit/5
        //public async Task<IActionResult> Edit(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var mainStoreStockRecord = await _context.MainStoreStockRecords.OrderByDescending(x => x.StockDate).SingleOrDefaultAsync(m => m.ID == id);
        //    if (mainStoreStockRecord == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["MainStoreID"] = new SelectList(_context.MainStores.Where(x => !x.IsDelete), "ID", "Name", mainStoreStockRecord.MainStoreID);
        //    ViewData["LiquorID"] = new SelectList(_context.Liquors.Where(x => !x.IsDelete).OrderBy(x => x.Code), "ID", "Name", mainStoreStockRecord.LiquorID);
        //    return View(mainStoreStockRecord);
        //}

        //// POST: MainStoreStockRecords/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(Guid id, MainStoreStockRecord mainStoreStockRecord)
        //{
        //    if (id != mainStoreStockRecord.ID)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            var data = _context.MainStoreStockRecords.Find(id);
        //            if (data != null)
        //            {
        //                var oldqty = data.QuantityInSmallestUnit;
        //                var mainStoreLiquor = _context.MainStoreLiquors.Where(x => x.LiquorID == mainStoreStockRecord.LiquorID && x.MainStoreID == mainStoreStockRecord.MainStoreID).SingleOrDefault();

        //                var packingUnits = _context.PackingUnits.Where(x => x.LiquorID == mainStoreStockRecord.LiquorID).ToList();
        //                mainStoreStockRecord.QuantityInSmallestUnit = mainStoreStockRecord.Quantity;
        //                mainStoreStockRecord.UnitName = "";
        //                if (packingUnits != null)
        //                {
        //                    var unitOrder = packingUnits.Where(x => x.UnitID == mainStoreStockRecord.UnitID).First().UnitOrder;
        //                    var qty = 1;
        //                    for (var i = packingUnits.Max(x => x.UnitOrder); i > unitOrder; i--)
        //                    {
        //                        qty *= packingUnits[i].QuantityInParent;
        //                    }
        //                    mainStoreStockRecord.QuantityInSmallestUnit *= qty;
        //                    mainStoreStockRecord.UnitName = packingUnits.Where(x => x.UnitID == mainStoreStockRecord.UnitID).First().Unit.Description;
        //                }

        //                if (mainStoreLiquor != null)
        //                {
        //                    mainStoreLiquor.Quantity -= oldqty;
        //                    mainStoreLiquor.Quantity += mainStoreStockRecord.QuantityInSmallestUnit;
        //                }
        //                else
        //                {
        //                    MainStoreLiquor newMainStoreLiquor = new MainStoreLiquor();
        //                    newMainStoreLiquor.ID = Guid.NewGuid();
        //                    newMainStoreLiquor.MainStoreID = mainStoreStockRecord.MainStoreID;
        //                    newMainStoreLiquor.LiquorID = mainStoreStockRecord.LiquorID;
        //                    newMainStoreLiquor.Quantity = mainStoreStockRecord.QuantityInSmallestUnit;
        //                    _context.MainStoreLiquors.Add(newMainStoreLiquor);
        //                }

        //                data.ID = mainStoreStockRecord.ID;
        //                data.MainStoreID = mainStoreStockRecord.MainStoreID;
        //                data.LiquorID = mainStoreStockRecord.LiquorID;
        //                data.StockDate = mainStoreStockRecord.StockDate;
        //                data.ExpiredDate = mainStoreStockRecord.ExpiredDate;
        //                data.Quantity = mainStoreStockRecord.Quantity;
        //                data.UnitID = mainStoreStockRecord.UnitID;
        //                data.QuantityInSmallestUnit = mainStoreStockRecord.QuantityInSmallestUnit;
        //                data.ModifiedBy = _userManager.GetUserId(User);
        //                await _context.SaveChangesAsync();
        //            }
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!MainStoreStockRecordExists(mainStoreStockRecord.ID))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["MainStoreID"] = new SelectList(_context.MainStores.Where(x => !x.IsDelete), "ID", "Name", mainStoreStockRecord.MainStoreID);
        //    ViewData["LiquorID"] = new SelectList(_context.Liquors.Where(x => !x.IsDelete).OrderBy(x => x.Code), "ID", "Name", mainStoreStockRecord.LiquorID);
        //    return View(mainStoreStockRecord);
        //}

        // GET: MainStoreStockRecords/Delete/5
        [Authorize(Roles = "Superadmin, Admin")]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mainStoreStockRecord = await _context.MainStoreStockRecords.OrderByDescending(x => x.StockDate)
                .Include(m => m.MainStore)
                .Include(m => m.Liquor)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (mainStoreStockRecord == null)
            {
                return NotFound();
            }

            return View(mainStoreStockRecord);
        }

        // POST: MainStoreStockRecords/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Superadmin, Admin")]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var mainStoreStockRecord = await _context.MainStoreStockRecords.OrderByDescending(x => x.StockDate).SingleOrDefaultAsync(m => m.ID == id);
            var mainStoreLiquor = _context.MainStoreLiquors.Where(x => x.LiquorID == mainStoreStockRecord.LiquorID && x.MainStoreID == mainStoreStockRecord.MainStoreID).SingleOrDefault();
            if (mainStoreLiquor != null)
            {
                mainStoreLiquor.Quantity -= mainStoreStockRecord.QuantityInSmallestUnit;
            }
            _context.MainStoreStockRecords.Remove(mainStoreStockRecord);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MainStoreStockRecordExists(Guid id)
        {
            return _context.MainStoreStockRecords.OrderByDescending(x => x.StockDate).Any(e => e.ID == id);
        }

        public JsonResult getUnitsByLiquor(Guid id) // Liquor ID
        {
            List<object> data = new List<object>();
            var packingUnits = _context.PackingUnits.Where(x => x.LiquorID == id).Include(x => x.Unit).ToList();
            foreach (var packingUnit in packingUnits.OrderBy(x => x.UnitOrder))
            {
                var obj = new
                {
                    id = packingUnit.UnitID.ToString(),
                    name = packingUnit.Unit.Description
                };
                data.Add(obj);
            }
            return Json(data);
        }
    }
}