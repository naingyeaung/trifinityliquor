function Receipt(id, type) {
    console.log(id);
    $.get("/Orders/ReceiptPdf?id=" + id + "&type=" + type, {}, function (data) {
        console.log(data);
        if (data != null && data != undefined) {
            var keyNames = Object.keys(data.items[0]);
            var items_columns = [];
            var items_rows = [];

            var invoiceinfolabel = '';
            var invoiceinfo = '';
            if (type == 0) { // OPD
                for (var i = 0; i < keyNames.length; i++) {
                    items_columns.push({ title: keyNames[i].toUpperCase(), dataKey: keyNames[i] });
                }
                items_rows = data.items.filter((e) => e.no != "title");

                invoiceinfolabel = '<p style="font-size: 12pt; margin: 0; text-align: right; line-height: 2;">'
                    + '<span>Date: <br>Voucher No: </span></p>';
                invoiceinfo = '<p style="font-size: 12pt; margin: 0; text-align: right; line-height: 2;">'
                    + '<span>' + data.date + '<br>' + data.voucherNo + '</span></p>';
            } else if (type == 1) { // IPD
                items_columns = [
                    { title: 'Day', dataKey: 'day' },
                    { title: 'Date', dataKey: 'date' },
                    { title: 'Total', dataKey: 'total' },
                    { title: 'Discount', dataKey: 'discount' },
                    { title: 'Balance', dataKey: 'balance' }
                ];
                var rows = data.items.filter((e) => e.no != "title");
                for (var i = 0; i < rows.length; i++) {
                    items_rows.push({ day: rows[i].code, date: rows[i].item, total: rows[i].price, discount: rows[i].qty, balance: rows[i].amount });
                }
                var hs = '-';
                var doa = data.date;
                var dodc = data.dodc;
                if (data.dodc != null && data.dodc != "" && data.dodc != undefined) {
                    hs = Math.round((new Date(data.dodc) - new Date(data.date)) / (1000 * 60 * 60 * 24)) + 1;
                } else {
                    dodc = '-';
                }

                invoiceinfolabel = '<p style="font-size: 11pt; margin: 0; text-align: right; line-height: 1.6;">'
                    + '<span>DOA: <br>DODC: <br>HS: <br>Voucher No: </span></p>';
                invoiceinfo = '<p style="font-size: 11pt; margin: 0; text-align: right; line-height: 1.6;">'
                    + '<span>' + doa + '<br>' + dodc + '<br>' + hs + '<br>' + data.voucherNo + '</span></p>';
            }

            // Only pt supported (not mm or in)
            var doc = new jsPDF('p', 'pt');
            var specialElementHandlers = {
                '#editor': function (element, renderer) {
                    return true;
                }
            };

            doc.setFontSize(10);

            var shopinfo = '<p style="margin: 5px 0px; line-height: 2;">'
                + '<b style= "font-size: 15pt;">Nay La Specialist Clinic</b></p>'
                + '<p style="font-size: 10pt; margin: 0; line-height: 1.6;">No.115, Pearl Street,<br/>Near War Memorial Cemetery,<br/>HtaukKyant<br/>09-771712300, 09-774899323, 09-444307024</p>';

            var invoicelogo = '<p style="margin: 5px 0px; text-align: right; line-height: 1.5; padding-top: 20px;">'
                + '<b style= "font-size: xx-large;">Invoice</b >'
                + '</p>';

            doc.fromHTML(shopinfo, 40, 15, {
                'width': doc.internal.pageSize.getWidth(),
                'elementHandlers': specialElementHandlers
            });

            doc.fromHTML(invoicelogo, doc.internal.pageSize.getWidth() / 2, 15, {
                'width': (doc.internal.pageSize.getWidth() / 2) - 40,
                'elementHandlers': specialElementHandlers
            });

            if (data.regID != null && data.regID != "") {
                var customerinfolabel = '<p style="font-size: 12pt; margin: 0; text-align: left; line-height: 2;">'
                    + '<span>Reg ID : <br>Patient : <br>Age :</span></p>';
                var customerinfo = '<p style="font-size: 12pt; margin: 0; text-align: left; line-height: 2;">'
                    + '<span>' + data.regID + '<br>' + data.patient + '<br>' + data.age + '</span></p>';
                doc.fromHTML(customerinfolabel, 40, 120, {
                    'width': (doc.internal.pageSize.getWidth() / 2) - 150,
                    'elementHandlers': specialElementHandlers
                });
                doc.fromHTML(customerinfo, 100, 120, {
                    'width': (doc.internal.pageSize.getWidth() / 4) + 150,
                    'elementHandlers': specialElementHandlers
                });
            }

            doc.fromHTML(invoiceinfolabel, 350, 120, {
                'width': 100,
                'elementHandlers': specialElementHandlers
            });
            doc.fromHTML(invoiceinfo, 445, 120, {
                'width': (doc.internal.pageSize.getWidth() / 4) - 40,
                'elementHandlers': specialElementHandlers
            });

            //doc.autoTable(custCol, custRow, {
            //    theme: 'plain',
            //    startY: 120,
            //    showHeader: 'never',
            //    columnStyles: {
            //        0: { fillColor: 255, columnWidth: 300 }
            //    },
            //});
            //let first = doc.autoTable.previous;
            doc.autoTable(items_columns, items_rows, {
                theme: 'grid',
                startY: 200,
                headerStyles: { fillColor: '#1f1f1f', halign: 'center' },
                bodyStyles: { textColor: '#1f1f1f' },
                columnStyles: {
                    // OPD
                    no: { halign: 'right' },
                    item: { columnWidth: 120, overflow: 'linebreak', overflowColumns: false },
                    price: { halign: 'right' },
                    qty: { halign: 'right' },
                    amount: { halign: 'right' },

                    // IPD
                    day: { halign: 'center' },
                    date: { halign: 'center' },
                    total: { halign: 'right' },
                    discount: { halign: 'right' },
                    balance: { halign: 'right' },
                },
                margin: { top: 60 },
                //addPageContent: function (data) {
                //    doc.setFont("Zawgyi-One");
                //    //doc.setFontType("bold");
                //    doc.setFontSize(20);
                //    var title = "А ну чики брики и в дамки!";
                //    var xOffset = (doc.internal.pageSize.getWidth() / 2) - (doc.getStringUnitWidth(title) * doc.internal.getFontSize() / 2);
                //    console.log(doc.internal.pageSize.getWidth());
                //    console.log(xOffset);
                //    doc.text(title, xOffset, 50);
                //}
            });
            doc.save(data.patient + ' (Voucher No: ' + data.voucherNo + ').pdf');
        }
    });
}

function DownloadLabResult(id) { // id = lab result's id
    $.get("/LabResults/LabResultPDF?id=" + id, {}, function (data) {
        if (data != null && data != undefined) {

            // Only pt supported (not mm or in)
            var doc = new jsPDF('p', 'pt');
            var specialElementHandlers = {
                '#editor': function (element, renderer) {
                    return true;
                }
            };

            doc.setFontSize(10);

            var shopinfo = '<p style="margin: 5px 0px; line-height: 1.5;">'
                + '<b style= "font-size: 15pt;">Nay La Specialist Clinic</b></p>'
                + '<p style="font-size: 10pt; margin: 0; line-height: 1.6;">No.115, Pearl Street,<br/>Near War Memorial Cemetery,<br/>HtaukKyant<br/>09-771712300, 09-774899323, 09-444307024</p>';

            var invoicelogo = '<p style="margin: 5px 0px; text-align: right; line-height: 1.5; padding-top: 20px;">'
                + '<b style= "font-size: xx-large;">' + data.labResult + '</b >'
                + '</p>';

            doc.fromHTML(shopinfo, 40, 15, {
                'width': doc.internal.pageSize.getWidth(),
                'elementHandlers': specialElementHandlers
            });

            doc.fromHTML(invoicelogo, doc.internal.pageSize.getWidth() / 2, 15, {
                'width': (doc.internal.pageSize.getWidth() / 2) - 40,
                'elementHandlers': specialElementHandlers
            });

            if (data.regID != null && data.regID != "") {
                var customerinfolabel = '<p style="font-size: 11pt; margin: 0; text-align: left; line-height: 1.5;">'
                    + '<span>Reg ID : <br>Patient : <br>Sex : <br>Age : </span></p>';
                var customerinfo = '<p style="font-size: 11pt; margin: 0; text-align: left; line-height: 1.5;">'
                    + '<span>' + data.regID + '<br>' + data.patient + '<br>' + data.sex + '<br>' + data.age + '</span></p>';
                doc.fromHTML(customerinfolabel, 40, 120, {
                    'width': (doc.internal.pageSize.getWidth() / 2) - 150,
                    'elementHandlers': specialElementHandlers
                });
                doc.fromHTML(customerinfo, 100, 120, {
                    'width': (doc.internal.pageSize.getWidth() / 3),
                    'elementHandlers': specialElementHandlers
                });
            }

            var invoiceinfolabel = '<p style="font-size: 11pt; margin: 0; text-align: right; line-height: 1.5;">'
                + '<span>Date: <br>Voucher No: <br>Reffered By: </span></p>';
            var invoiceinfo = '<p style="font-size: 11pt; margin: 0; text-align: right; line-height: 1.5;">'
                + '<span>' + data.date + '<br>' + data.labResultNo + '<br>' + data.refferedBy + '</span></p>';
            doc.fromHTML(invoiceinfolabel, 350, 120, {
                'width': 100,
                'elementHandlers': specialElementHandlers
            });
            doc.fromHTML(invoiceinfo, 445, 120, {
                'width': (doc.internal.pageSize.getWidth() / 4) - 40,
                'elementHandlers': specialElementHandlers
            });

            if (data.isLabReport) {
                doc.line(40, 200, doc.internal.pageSize.getWidth() - 40, 200)
                var gross = '<p style="font-size: 12pt; margin: 0; text-align: justify; line-height: 2;">'
                    + '<span><b>Gross</b> - ' + (data.gross != null ? data.gross : "") + '</span></p>';
                doc.fromHTML(gross, 40, 200, {
                    'width': (doc.internal.pageSize.getWidth() - 80),
                    'elementHandlers': specialElementHandlers
                });

                var labPhoto1 = data.labPhoto1;
                var labPhoto2 = data.labPhoto2;
                var labPhoto3 = data.labPhoto3;
                var reportBodyYPosition = 250;
                var remarkYPosition = 350;
                if ((labPhoto1 != null && labPhoto1 != "" && labPhoto1 != undefined) || (labPhoto2 != null && labPhoto2 != "" && labPhoto2 != undefined) || (labPhoto3 != null && labPhoto3 != "" && labPhoto3 != undefined)) {
                    if (labPhoto1 != null && labPhoto1 != "" && labPhoto1 != undefined) {
                        doc.addImage(labPhoto1, 40, 250, 150, 150);
                        doc.text(60, 415, data.reportBodyName + ' (Low Power)')
                    }
                    if (labPhoto2 != null && labPhoto2 != "" && labPhoto2 != undefined) {
                        doc.addImage(labPhoto2, 222, 250, 150, 150);
                        doc.text(242, 415, data.reportBodyName + ' (High Power)')
                    }
                    if (labPhoto3 != null && labPhoto3 != "" && labPhoto3 != undefined) {
                        doc.addImage(labPhoto3, 400, 250, 150, 150);
                        doc.text(420, 415, data.reportBodyName + ' (High Power)')
                    }

                    reportBodyYPosition = 420;
                    remarkYPosition = 520;
                }

                var reportBodyName = '<p style="font-size: 12pt; margin: 0; text-align: left; line-height: 2;">'
                    + '<span><b>' + data.reportBodyName + '</b> - ' + (data.reportBody != null ? data.reportBody : "") + '</span></p>';
                doc.fromHTML(reportBodyName, 40, reportBodyYPosition, {
                    'width': (doc.internal.pageSize.getWidth() - 80),
                    'elementHandlers': specialElementHandlers
                });

                var remark = '<p style="font-size: 12pt; margin: 0; text-align: left; line-height: 2;">'
                    + '<span><b>Remark</b> - ' + (data.remark != null ? data.remark : "") + '</span></p>';
                doc.fromHTML(remark, 40, remarkYPosition, {
                    'width': (doc.internal.pageSize.getWidth() - 80),
                    'elementHandlers': specialElementHandlers
                });
            } else {
                var items_columns = [];
                console.log(data.items);
                if (data.items != null && data.items != undefined && data.items.length > 0) {
                    var keyNames = Object.keys(data.items[0]);
                    for (var i = 0; i < keyNames.length; i++) {
                        items_columns.push({ title: keyNames[i].toUpperCase(), dataKey: keyNames[i] });
                    }
                } else {
                    items_columns = [
                        { title: 'Test', dataKey: 'test' },
                        { title: 'Result', dataKey: 'result' },
                        { title: 'Unit', dataKey: 'unit' },
                        { title: 'Reference Range', dataKey: 'referenceRange' },
                        { title: 'Remark', dataKey: 'remark' }
                    ];
                }

                var items_rows = data.items.filter((e) => e.no != "title");

                doc.autoTable(items_columns, items_rows, {
                    theme: 'grid',
                    startY: 200,
                    headerStyles: { fillColor: '#1f1f1f', halign: 'center' },
                    bodyStyles: { textColor: '#1f1f1f' },
                    columnStyles: {
                        test: { halign: 'left' },
                        result: { columnWidth: 120, overflow: 'linebreak', overflowColumns: false },
                        unit: { halign: 'center' },
                        referenceRange: { halign: 'center' },
                        remark: { columnWidth: 120, overflow: 'linebreak', overflowColumns: false },
                    },
                    margin: { top: 60 },
                });

                doc.setFontSize(12);
                doc.text("Remark", 40, doc.autoTable.previous.finalY + 25);

                doc.setFontSize(10);
                var splitTitle = doc.splitTextToSize((data.remark != null ? data.remark : ""), (doc.internal.pageSize.getWidth() - 80));
                doc.text(40, doc.autoTable.previous.finalY + 50, splitTitle);
            }

            doc.save(data.patient + ' (LabResult No: ' + data.labResultNo + ').pdf');
        }
    });
}