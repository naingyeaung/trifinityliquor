
class TransferLiquors extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: props.value,
            allMainStoreLiquors: allMainStoreLiquors,
            transferLiquors: transferLiquors,
        };

        this.changeUnit = this.changeUnit.bind(this);
        this.changeInput = this.changeInput.bind(this);
        this.addUnit = this.addUnit.bind(this);
    }

    changeUnit(e) {
        var value = e.target.value;
        $('.unitToAdd').val(value);
    }

    changeInput(e) {
        var id = e.target.id;
        var value = e.target.value;
        for (var i = this.state.transferLiquors.length - 1; i >= 0; i--) {
            if ("Quantity" + this.state.transferLiquors[i].medicineID === id) {
                this.state.transferLiquors[i].qty = value;
                this.state.transferLiquors[i].qtyInSmallestUnit = this.calculateQtyInSmallestUnit(this.state.transferLiquors[i]);
                if (this.checkQty(this.state.transferLiquors[i])) {
                    this.state.transferLiquors[i].qty = 0;
                    this.state.transferLiquors[i].qtyInSmallestUnit = 0;
                    //$('.qtyexceeded' + this.state.transferLiquors[i].medicineID).show();
                    alert('Quantity limit is exceeded.');
                }
                this.setState({ transferLiquors: this.state.transferLiquors });
                return false;
            }
            if ("Unit" + this.state.transferLiquors[i].medicineID === id) {
                var unit = this.state.transferLiquors[i].units.find(function (element) {
                    return element.id === value;
                });
                this.state.transferLiquors[i].unitID = unit.id;
                this.state.transferLiquors[i].unitName = unit.name;
                this.state.transferLiquors[i].qtyInSmallestUnit = this.calculateQtyInSmallestUnit(this.state.transferLiquors[i]);
                if (this.checkQty(this.state.transferLiquors[i])) {
                    this.state.transferLiquors[i].qty = 0;
                    this.state.transferLiquors[i].qtyInSmallestUnit = 0;
                    //$('.qtyexceeded' + this.state.transferLiquors[i].medicineID).show();
                    alert('Quantity limit is exceeded.');
                }
                this.setState({ transferLiquors: this.state.transferLiquors });
                return false;
            }
        }
    }

    calculateQtyInSmallestUnit(medicine) {
        var qty = 1;
        medicine.qtyInSmallestUnit = medicine.qty;
        var currentUnit = medicine.units.find(function (element) {
            return element.id === medicine.unitID;
        });
        for (var i = Math.max.apply(Math, medicine.units.map(function (o) { return o.unitOrder; })); i > currentUnit.unitOrder; i--) {
            qty *= medicine.units[i - 1].qtyInParent;
        }
        medicine.qtyInSmallestUnit *= qty;
        return medicine.qtyInSmallestUnit;
    }

    checkQty(medicine) {
        $('.qtyexceeded').hide();
        if (medicine.qtyInSmallestUnit > medicine.totalQty)
            return 1;
        else
            return 0;
    }

    addUnit() {
        var id = document.getElementById("unitToAddID").value;
        var unit = null;
        for (var i = this.state.allMainStoreLiquors.length - 1; i >= 0; i--) {
            if (this.state.allMainStoreLiquors[i].medicineID === id) {
                unit = this.state.allMainStoreLiquors[i];
                this.state.allMainStoreLiquors.splice(i, 1);
            }
        }
        this.setState({ allMainStoreLiquors: this.state.allMainStoreLiquors });
        if (unit !== null) {
            unit.qty = 0;
            unit.unitID = unit.units[0].id;
            unit.unitName = unit.units[0].name;
            unit.qtyInSmallestUnit = this.calculateQtyInSmallestUnit(unit);
            if (this.checkQty(unit)) {
                unit.qty = 0;
                unit.qtyInSmallestUnit = 0;
                $('.qtyexceeded' + unit.medicineID).show();
            }
            this.state.transferLiquors.push(unit);
            this.setState({ transferLiquors: this.state.transferLiquors });
        }
    }

    deleteUnit(id) {
        var unit = null;
        for (var i = this.state.transferLiquors.length - 1; i >= 0; i--) {
            if (this.state.transferLiquors[i].medicineID === id) {
                unit = this.state.transferLiquors[i];
                this.state.transferLiquors.splice(i, 1);
            }
        }
        this.setState({ transferLiquors: this.state.transferLiquors });
        if (unit !== null) {
            this.state.allMainStoreLiquors.push(unit);
            this.setState({ allMainStoreLiquors: this.state.allMainStoreLiquors });
        }
    }

    render() {
        return (
            <div className="form-group row px-3">
                <label className="col-12 px-0">Transfer Liquors</label>
                <div className="row w-100">
                    <div className="col-8">
                        <div className="row">
                            <div className="col-5">
                                <select className="form-control react-input-1 hideInDetail unitToAdd" id="unitToAddID" onChange={this.changeUnit}>
                                    {
                                        this.state.allMainStoreLiquors.map(item =>
                                            <option key={item.medicineID + "1"} value={item.medicineID}>
                                                {item.medicineCode}
                                            </option>
                                        )
                                    }
                                </select>
                            </div>
                            <div className="col-5">
                                <select className="form-control react-input-1 hideInDetail unitToAdd" onChange={this.changeUnit}>
                                    {
                                        this.state.allMainStoreLiquors.map(item =>
                                            <option key={item.medicineID + "2"} value={item.medicineID}>
                                                {item.medicineName}
                                            </option>
                                        )
                                    }
                                </select>
                            </div>
                            <div className="col-1">
                                <a onClick={this.addUnit} className="form-control px-2 pr-4 mx-1"><i className="nc-icon nc-simple-add"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={"col-8 my-2 px-1" + (this.state.transferLiquors.length === 0 ? " d-none" : "")}>
                    <label className="col-1">No.</label>
                    <label className="col-2">Code</label>
                    <label className="col-3">Name</label>
                    <label className="col-2">Qty</label>
                    <label className="col-3">Unit</label>
                    <label className="col-1"></label>
                </div>
                <ul className="list-group col-8 mb-2 px-0">
                    {
                        this.state.transferLiquors.map((item, i) =>
                            <li id={item.medicineID} key={i} className="list-group-item">
                                <div className="row px-0">
                                    <input type="hidden" name={"TransferLiquors[" + i + "].LiquorID"} value={item.medicineID} />
                                    <input type="hidden" name={"TransferLiquors[" + i + "].Quantity"} value={item.qty} />
                                    <input type="hidden" name={"TransferLiquors[" + i + "].UnitID"} value={item.unitID} />
                                    <input type="hidden" name={"TransferLiquors[" + i + "].UnitName"} value={item.unitName} />
                                    <input type="hidden" name={"TransferLiquors[" + i + "].QuantityInSmallestUnit"} value={item.qtyInSmallestUnit} />
                                    <div className="col-1 py-2">{i + 1}</div>
                                    <div className="col-2 py-2">{item.medicineCode}</div>
                                    <div className="col-3 py-2">{item.medicineName}</div>
                                    <div className="col-2">
                                        <input type="number" id={"Quantity" + item.medicineID} onChange={this.changeInput} value={item.qty} className="form-control" />
                                        <span className={"text-danger d-none qtyexceeded qtyexceeded" + item.medicineID}>Qty exceeded</span>
                                    </div>
                                    <div className="col-3">
                                        <select className="form-control packingunit" id={"Unit" + item.medicineID} onChange={this.changeInput}>
                                            {
                                                item.units.map((unit, j) =>
                                                    <option key={j} value={unit.id}>{unit.name}</option>
                                                )
                                            }
                                        </select>
                                    </div>
                                    <div className="col-1 py-2">
                                        <i onClick={() => this.deleteUnit(item.medicineID)} className="nc-icon nc-simple-remove pull-right"></i>
                                    </div>
                                </div>
                            </li>
                        )
                    }
                </ul>
                {/*<div className="row w-100">
                    {
                        this.state.transferLiquors.map((item, i) =>
                            <div className="col-4">
                                <span>Liquor ID : {item.medicineID}</span> <br />
                                <span>Liquor Code : {item.medicineCode}</span> <br />
                                <span>Liquor Name : {item.medicineName}</span> <br />
                                <span>Total Qty in Main Store : {item.totalQty}</span> <br />
                                <span>Qty : {item.qty}</span> <br />
                                <span>Unit ID : {item.unitID}</span> <br />
                                <span>Unit Name : {item.unitName}</span> <br />
                                <span>Qty in Smallest Unit : {item.qtyInSmallestUnit}</span> <br /><br />
                            </div>
                        )
                    }
                </div> */}
                {/* <span>{this.state.UnitPrice}<br />{this.state.PercentageForSale} %<br />{(this.state.UnitPrice * this.state.PercentageForSale) / 100} kyats<br />{JSON.stringify(this.state.transferLiquors)}</span> */}
            </div>
        );
    }
}
ReactDOM.render(
    <TransferLiquors />, document.getElementById('root'));
