﻿class ServiceFee extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            allLabPersonTypes: [],
            allLabPersons: [],
            currentType: 1,
            virtualLabPersons: virtualLabPersons,
            referralFeeType: referralFeeType,
            referralFee: referralFee,
            permitted: true
        };

        this.addUnit = this.addUnit.bind(this);
    }

    componentWillMount() {
        var serviceFee = this;
        $.get('/LabPersons/GetLabPersons', function (data) {
            if (data != null && data != undefined) {
                if (data.labPersons != null && data.labPersons != undefined) {
                    serviceFee.setState({ allLabPersons: data.labPersons });
                }
                if (data.labPersonTypes != null && data.labPersonTypes != undefined) {
                    data.labPersonTypes.sort(function (a, b) { return (a.type > b.type) ? 1 : ((b.type > a.type) ? -1 : 0); });
                    serviceFee.setState({ allLabPersonTypes: data.labPersonTypes });
                }
                serviceFee.reloadLabPersons();
            }
        });
    }

    componentDidMount() {
        this.reloadLabPersons();
        this.setState({ referralFeeType: referralFeeType, referralFee: referralFee });
    }

    componentDidUpdate() {

    }

    reloadLabPersons() {
        for (var i = 0; i < virtualLabPersons.length; i++) {
            var index = this.state.allLabPersons.findIndex(x => x.id == virtualLabPersons[i].id);
            if (index != -1) {
                this.state.allLabPersons.splice(index, 1);
            }
        }
        this.setState({ allLabPersons: JSON.parse(JSON.stringify(this.state.allLabPersons)) });
        this.resortLabPersons();
    }

    resortLabPersons() {
        this.state.allLabPersons.sort(function (a, b) { return (a.name < b.name) ? 1 : ((b.name < a.name) ? -1 : 0); })
        this.setState({ allLabPersons: this.state.allLabPersons });
    }

    addUnit() {
        var id = document.getElementById("unitToAddID").value;
        var unit = null;
        var index = this.state.allLabPersons.findIndex(x => x.id == id);
        if (index != -1) { // Found in array
            unit = this.state.allLabPersons[index];
            this.state.allLabPersons.splice(index, 1);
            this.setState({ allLabPersons: this.state.allLabPersons });
            this.state.virtualLabPersons.push(unit);
            this.setState({ virtualLabPersons: this.state.virtualLabPersons });
        }
    }

    deleteUnit(id) {
        var unit = null;
        var index = this.state.virtualLabPersons.findIndex(x => x.id == id);
        if (index != -1) { // Found in array
            unit = this.state.virtualLabPersons[index];
            this.state.virtualLabPersons.splice(index, 1);
            this.setState({ virtualLabPersons: this.state.virtualLabPersons });
            this.state.allLabPersons.push(unit);
            this.setState({ allLabPersons: this.state.allLabPersons });
            this.resortLabPersons();
        }
    }

    render() {
        return (
            <div className="form-group row px-3">
                <label className="col-12">Service Fees</label>
                <div className="col-4">
                    <label>Type</label>
                    <select className={"form-control react-input-1 pull-left"} onChange={(e) => this.setState({ currentType: e.target.value })}>
                        {
                            this.state.allLabPersonTypes.map(item =>
                                <option key={item.type} value={item.type}>
                                    {item.typeName}
                                </option>
                            )
                        }
                    </select>
                </div>
                <div className="col-4">
                    <label>Name</label>
                    <select className={"form-control react-input-1 pull-left"} id="unitToAddID">
                        {
                            this.state.allLabPersons.filter(x => x.type == this.state.currentType).map(item =>
                                <option key={item.id} value={item.id}>
                                    {item.name}
                                </option>
                            )
                        }
                    </select>
                </div>
                <div className="col-2">
                    <center>
                        <label></label>
                        <a onClick={this.addUnit} style={{ width: "50" }} className={"form-control nav-link"}><i className="nc-icon nc-simple-add"></i></a>
                    </center>
                </div>

                <div className={"col-12 my-2 mt-3 px-1"}>
                    <div className="row mx-3">
                        <label className="col-2">Code</label>
                        <label className="col-2">Name</label>
                        <label className="col-4">FeeType</label>
                        <label className="col-2">Amount</label>
                        <label className="col-1"></label>
                    </div>
                </div>
                <ul className="list-group col-12 mb-2 px-0">
                    <li className="list-group-item">
                        <label className="col-2">Ref</label>
                        <label className="col-2">Referral</label>
                        <label className="col-4">
                            <select className="form-control" style={{ width: "150" }} name={"ReferralFeeType"} value={this.state.referralFeeType} onChange={(e) => this.setState({ referralFeeType: e.target.value })}>
                                <option value="1">Fixed Amount</option>
                                <option value="2">Percentage</option>
                            </select>
                        </label>
                        <label className="col-3"><input type="number" name={"ReferralFee"} value={this.state.referralFee} onChange={(e) => this.setState({ referralFee: e.target.value })} className="form-control number" min="0" max={this.state.referralFeeType == 2 ? "100" : ""} required /></label>
                        <label className="col-1 pt-1"></label>
                    </li>
                    {
                        this.state.virtualLabPersons.map((item, i) =>
                            <li id={item.ID} key={i} className="list-group-item">
                                <input type="hidden" name={"LabPerson_Provide_Services[" + i + "].LabPersonID"} value={item.id} />
                                <input type="hidden" name={"LabPerson_Provide_Services[" + i + "].SortOrder"} value={i + 1} />
                                <label className="col-2">{item.code}</label>
                                <label className="col-2">{item.name}</label>
                                <label className="col-4">
                                    <select className="form-control" style={{ width: "150" }} name={"LabPerson_Provide_Services[" + i + "].FeeType"} value={item.feeType} onChange={(e) => { item.feeType = e.target.value; this.setState({ virtualLabPersons: this.state.virtualLabPersons }) }}>
                                        <option value="1">Fixed Amount</option>
                                        <option value="2">Percentage</option>
                                    </select>
                                </label>
                                <label className="col-3"><input type="number" name={"LabPerson_Provide_Services[" + i + "].Amount"} value={item.fee} onChange={(e) => { item.fee = e.target.value; this.setState({ virtualLabPersons: this.state.virtualLabPersons }) }} className="form-control number" min="0" max={item.feeType == 2 ? "100" : ""} /></label>
                                <label className="col-1 pt-1"><i onClick={() => this.deleteUnit(item.id)} style={{ marginBottom: "-5" }} className="nc-icon nc-simple-remove pull-right"></i></label>
                            </li>
                        )
                    }
                </ul>
                {/* <span>{this.state.UnitPrice}<br />{this.state.PercentageForSale} %<br />{(this.state.UnitPrice * this.state.PercentageForSale) / 100} kyats<br />{JSON.stringify(this.state.virtualpackingunits)}</span> */}
            </div>
        );
    }
}

ReactDOM.render(
    <ServiceFee ref={(serviceFeeComponent => window.serviceFeeComponent = serviceFeeComponent)} />, document.getElementById('root'));