﻿class Order extends React.Component {
    constructor(props) {
        super(props);
        //orderOutletLiquor.sort(function (a, b) { return (a.sortorder > b.sortorder) ? 1 : ((b.sortorder > a.sortorder) ? -1 : 0); })
        this.state = {
            orderID: orderid,
            allLiquors: [],
            orderLiquors: [],
            total: total,
            discount: discount,
            paid: 0,
            change : 0,
            balance: balance,
            addorder: 0,
            permitted: permitted,
            permitPharmacy: permitPharmacy,
            permitService: permitService,
            notFromDB: true,
        };

        this.addUnit = this.addUnit.bind(this);
        this.changeLiquor = this.changeLiquor.bind(this);
        this.calculateAmount = this.calculateAmount.bind(this);
        this.changeInput = this.changeInput.bind(this);
        this.getLiquorByBarcode = this.getLiquorByBarcode.bind(this);
        this.changePaid = this.changePaid.bind(this);
        this.calculateChange = this.calculateChange.bind(this);


        Array.prototype.sum = function (prop) {
            var total = 0
            for (var i = 0, _len = this.length; i < _len; i++) {
                total += this[i][prop]
            }
            return total
        }
    }

    handleFocus = (event) => event.target.select();

    componentWillMount() {
        var thisprop = this;

        var outletid = $(OutletID).val();
        $.get('/Order/GetLiquorsByOutlet?id=' + outletid + "&orderid=" + orderid, (data) => {
            this.setState({ allLiquors: data });
        });


        //}



        $('select.liquor').each(function (i, j) {
            if (!$(this).hasClass('select2-hidden-accessible')) {
                $(this).select2();
                $(this).on('select2:select', function (e) {
                    thisprop.changeLiquor(e);
                });
            }
        });

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // Typical usage (don't forget to compare props):
        var methods = this;
        if (this.state.addorder !== prevState.addorder) {
            $('select.liquor').each(function (i, j) {
                if (!$(this).hasClass('select2-hidden-accessible')) {
                    $(this).select2();
                    $(this).on('select2:select', function (e) {
                        methods.changeLiquor(e);
                    });
                }
            });
        }


        $('select.liquor').trigger('change');


    }

    compare(a, b) {
        if (a.unitOrder < b.unitOrder)
            return -1;
        if (a.unitOrder > b.unitOrder)
            return 1;
        if (a.unitOrder === b.unitOrder) {
            if (a.name < b.name)
                return -1;
            if (a.name > b.name)
                return 1;
        }
        return 0;
    }

    calculateQtyInSmallestUnit(liquor) {
        var qty = 1;
        liquor.qtyInSmallestUnit = liquor.qty;
        var currentUnit = liquor.units.find(function (element) {
            return element.id === liquor.packingUnitID;
        });
        for (var i = Math.max.apply(Math, liquor.units.map(function (o) { return o.unitOrder; })); i > currentUnit.unitOrder; i--) {
            qty *= liquor.units[i - 1].qtyInParent;
        }
        liquor.qtyInSmallestUnit *= qty;
        return liquor.qtyInSmallestUnit;
    }

    checkQty(liquor) {
        //var meds = this.state.orderLiquors.filter(m => m.liquorID === liquor.liquorID);
        //var allmeds = this.state.allLiquors.filter(m => m.liquorID === liquor.liquorID);
        //var totalSmallestQty = meds.sum("qtyInSmallestUnit");
        //var totalQtyInOutlet = allmeds[0].totalQty;
        //if (totalSmallestQty > totalQtyInOutlet)
        //    return 1;
        //else
        //    return 0;

        return 0;
    }

    changeLiquor(e) {
        if (this.state.permitted || this.state.permitPharmacy) {
            var x = e.target;
            $('select.liquor' + x.title).val(x.value).trigger('change');
            var med = this.state.allLiquors.filter(m => m.liquorID === $('#liquorid' + x.title).val());
            this.state.orderLiquors[x.title - 1] = JSON.parse(JSON.stringify(med[0]));
            this.state.orderLiquors[x.title - 1].amount = parseInt("0");
            $('#unitid' + x.title).val(this.state.orderLiquors[x.title - 1].units[this.state.orderLiquors[x.title - 1].units.length-1].id);
            this.state.orderLiquors[x.title - 1].packingUnitID = this.state.orderLiquors[x.title - 1].units[this.state.orderLiquors[x.title - 1].units.length-1].id;
            this.setState({ orderLiquors: this.state.orderLiquors });
            this.calculateAmount(x.title - 1);
        }
    }



    changeUnit(i) {
        if (this.state.permitted || this.state.permitPharmacy) {
            this.state.orderLiquors[i].packingUnitID = $('#unitid' + (i + 1)).val();
            this.calculateAmount(i);
        }
    }



    calculateAmount(i) {
        this.state.orderLiquors[i].qtyInSmallestUnit = this.calculateQtyInSmallestUnit(this.state.orderLiquors[i]);
        this.state.orderLiquors[i].unitPrice = parseInt(this.state.orderLiquors[i].units.filter(m => m.id == this.state.orderLiquors[i].packingUnitID)[0].saleamount);
        this.state.orderLiquors[i].amount = 0;
        if (this.checkQty(this.state.orderLiquors[i])) {
            this.state.orderLiquors[i].qty = 0;
            this.state.orderLiquors[i].qtyInSmallestUnit = 0;
            alert('Quantity limit is exceeded.');
        } else {
            this.state.orderLiquors[i].amount = this.state.orderLiquors[i].unitPrice * parseInt(((!isNaN(this.state.orderLiquors[i].qty) && this.state.orderLiquors[i].qty != null && this.state.orderLiquors[i].qty != '') ? this.state.orderLiquors[i].qty : 0));
        }
        this.calculateTotal();
        this.setState({ orderLiquors: this.state.orderLiquors });
    }

    async changeInput(e) {
        var x = e.target;

            var qty = x.value;
            qty = qty < 0 ? 0 : qty;
            this.state.orderLiquors[x.title - 1].qty = parseInt(qty);
            this.calculateAmount(x.title - 1);
    }




    addUnit() { // add liquor
        if (this.state.permitted || this.state.permitPharmacy) {
            if (this.state.allLiquors.length > 0) {
                var sortorder = this.state.orderLiquors.length + 1;
                this.state.orderLiquors.push(JSON.parse(JSON.stringify(this.state.allLiquors[0])));
                $('#unitid' + sortorder).val(this.state.orderLiquors[sortorder - 1].units[0].id);
                this.state.orderLiquors[sortorder - 1].packingUnitID = this.state.orderLiquors[sortorder - 1].units[this.state.orderLiquors[sortorder - 1].units.length-1].id;
                this.setState({ orderLiquors: this.state.orderLiquors });
                this.calculateAmount((sortorder - 1));
                this.setState({ addorder: this.state.addorder + 1 });
                //this.sortUnit();
            }
        }
    }



    deleteUnit(i) {
        if (this.state.permitted || this.state.permitPharmacy) {
            this.state.orderLiquors.splice(i, 1);
            this.setState({ orderLiquors: this.state.orderLiquors });
            this.calculateTotal();
        }
    }


    calculateTotal() {
        this.state.total = this.state.orderLiquors.sum("amount");
   
        this.state.balance = this.state.total;
        this.setState({ total: this.state.total, balance: this.state.balance });
        this.calculateChange();
    }

    getLiquorByBarcode(e) {

        var x = e.target;
        if (e.keyCode == 13) {
            e.preventDefault()
            var barcode = $("#barcodeid" + x.title).val();
            var med = this.state.allLiquors.filter(m => m.barcode === barcode);
            this.state.orderLiquors[x.title - 1] = JSON.parse(JSON.stringify(med[0]));
            this.state.orderLiquors[x.title - 1].amount = parseInt("0");
            $('#unitid' + x.title).val(this.state.orderLiquors[x.title - 1].units[this.state.orderLiquors[x.title - 1].units.length-1].id);
            this.state.orderLiquors[x.title - 1].packingUnitID = this.state.orderLiquors[x.title - 1].units[this.state.orderLiquors[x.title - 1].units.length-1].id;
            this.setState({ orderLiquors: this.state.orderLiquors });
            this.calculateAmount(x.title - 1);
        }   
    }

    barchodeChange(e) {
        var x = e.target;
        var barcode = $("#barcodeid" + x.title).val();
        this.state.orderLiquors[x.title - 1].barcode = barcode;
        this.setState({ orderLiquors: this.state.orderLiquors });
    }

    changePaid(e) {
        this.state.paid = parseInt(e.target.value);
        this.setState({ paid: this.state.paid });
        this.calculateChange();
        //var ths = this;
        //setTimeout(function () {
        //    ths.calculateChange();
        //}, 1000);
        
    }

    calculateChange() {
        var total = this.state.total;
        var paid = this.state.paid;
        console.log(total);
        console.log(paid);
        this.state.change = paid - total;
        this.state.change = this.state.change >= 0 ? this.state.change : 0;
        this.setState({ change: this.state.change });

    }

    compareUnit(a, b) { //to sort unit
        if (a.unitOrder > b.unitOrder) {
            return -1;
        }
        if (a.last_nom < b.last_nom) {
            return 1;
        }
        return 0;
    }

    sortUnit() {
        for (var i = 0; i < this.state.orderLiquors.length; i++) {
            this.state.orderLiquors[i].units.sort(this.compareUnit);
            this.setState({ orderLiquors: this.state.orderLiquors });
        }
    }

    render() {
        return (
            <div className="row table-responsive mx-0 my-3">
                {/*<div className="row w-100">
                    {
                        this.state.orderLiquors.map((item, i) =>

                            <div className="col-4" key={i}>
                                <span>Liquor ID : {item.liquorID}</span> <br />
                                <span>Liquor Name : {item.liquorName}</span> <br />
                                <span>Unit Price : {item.unitPrice}</span> <br />
                                <span>Total Qty in Main Store : {item.totalQty}</span> <br />
                                <span>Qty : {item.qty}</span> <br />
                                <span>Amount : {item.amount}</span> <br />
                                <span>Sort Order : {item.sortorder}</span> <br />
                                <span>Qty in Smallest Unit : {item.qtyInSmallestUnit}</span> <br /><br />
                            </div>

                        )
                    }
                </div>*/}
                <table className="table table-bordered table-condensed">
                    <thead className={this.state.permitted || this.state.permitPharmacy ? "" : "disable-order-panel"}>
                        <tr className="react-table-header"><th colSpan="9" className="TitleType">Liquor</th></tr>
                        <tr className="react-table-header" style={{ border: "1px solid #e9ecef" }}>
                            <th className="w-5">No.</th>
                            <th className="w-20">BarCode</th>
                            <th className="w-20">Code</th>
                            <th className="w-20">Items</th>
                            <th className="w-15">Unit</th>
                            <th className="w-10">Unit Price</th>
                            <th className="w-10">Qty</th>
                            <th className="w-15">Amount</th>
                            <th className="w-5 add_delete_unit" onClick={this.addUnit}><i className="nc-icon nc-simple-add"></i></th>
                        </tr>
                    </thead>
                    <tbody className={this.state.permitted || this.state.permitPharmacy ? "" : "disable-order-panel"}>
                        {
                            this.state.orderLiquors.map((item, i) =>
                                <tr key={i}>
                                    <td className="number">
                                        <input type="hidden" name={"OrderOutletLiquors[" + i + "].SortOrder"} value={i} />
                                        <input type="hidden" name={"OrderOutletLiquors[" + i + "].QuantityInSmallestUnit"} value={item.qtyInSmallestUnit} />
                                        {i + 1}.
                                    </td>
                                    <td>
                                        <input type="text" className="form-control" value={item.barcode} title={i + 1} id={"barcodeid" + (i + 1)} onKeyDown={(e) => this.getLiquorByBarcode(e)} onChange={(e) => this.barchodeChange(e)} autoFocus={true} onClick={this.handleFocus} />
                                    </td>
                                    <td className="hasSelect2">
                                        <select className={"form-control liquor liquor" + (i + 1)} value={item.liquorID} title={i + 1} id={"liquorid" + (i + 1)} name={"OrderOutletLiquors[" + i + "].LiquorID"} onChange={(e) => changeLiquor(e)} >
                                            {
                                                this.state.allLiquors.map((liquor, m) =>
                                                    <option key={liquor.liquorID} value={liquor.liquorID}>{liquor.liquorCode}</option>
                                                )
                                            }
                                        </select>
                                    </td>
                                    <td className="hasSelect2">
                                        <select className={"form-control liquor liquor" + (i + 1)} value={item.liquorID} title={i + 1} onChange={(e) => changeLiquor(e)} >
                                            {
                                                this.state.allLiquors.map((liquor, m) =>
                                                    <option key={m} value={liquor.liquorID}>{liquor.liquorName}</option>
                                                )
                                            }
                                        </select>
                                    </td>
                                    <td>
                                        <select className="form-control packingunit" value={item.packingUnitID} id={"unitid" + (i + 1)} name={"OrderOutletLiquors[" + i + "].UnitID"} onChange={() => this.changeUnit(i)}>
                                            {
                                                item.units.map((unit, j) =>
                                                    <option key={unit.id} value={unit.id}>{unit.name}</option>
                                                )
                                            }
                                        </select>
                                    </td>
                                    <td className="number"><input type="hidden" id={"unitpriceid" + (i + 1)} name={"OrderOutletLiquors[" + i + "].UnitPrice"} value={item.unitPrice} /><span>{item.unitPrice}</span></td>
                                    <td><input min="0" className="form-control number detailnumber" type="number" required id={"qtyid" + (i + 1)} name={"OrderOutletLiquors[" + i + "].Quantity"} value={item.qty} title={i + 1} onChange={this.changeInput} /></td>
                                    <td className="number amount"><input type="hidden" id="" name={"OrderOutletLiquors[" + i + "].Amount"} value={item.amount} /><span>{item.amount}</span></td>
                                    <td className="add_delete_unit" onClick={() => this.deleteUnit(i)}><i className="nc-icon nc-simple-remove"></i></td>
                                </tr>
                            )
                        }

                        {
                            this.state.orderLiquors.length > 0 ?
                                (<React.Fragment>
                                    <tr>
                                        <td></td>
                                        <td colSpan='8' className='add_delete_unit'><button type='button' className='btn' onClick={this.addUnit}>Add New Liquor</button></td>
                                    </tr>
                                </React.Fragment>) : (<React.Fragment></React.Fragment>)
                        }
                    </tbody>
                    
                    <tfoot>
                        <tr>
                            <td colSpan={5}></td>
                            <td>Total</td>
                            <td colSpan={2} className="number amount"><input type="hidden" name="Total" value={this.state.total} />{this.state.total}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colSpan={5}></td>
                            <td>Paid</td>
                            <td colSpan={2}><input type="hidden" name="Balance" value={this.state.balance} /><input style={{ textAlign: "right" }} className="form-control" id="paid" type="number" value={this.state.paid} onChange={(e) => this.changePaid(e)} /></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colSpan={5}></td>
                            <td>Change</td>
                            <td colSpan={2} className="number amount">{this.state.change}</td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        )
    }
}

ReactDOM.render(
    <Order ref={(orderComponent) => { window.orderComponent = orderComponent }} />, document.getElementById('root'));