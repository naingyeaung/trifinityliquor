class LabResult extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            serviceTypes: [],
            serviceTypeID: serviceTypeID != null ? serviceTypeID : undefined,
            labResults: [],
            action: action,
            labResultID: labResultID,
            isLabReport: undefined,
            reportBodyName: undefined
        }

        this.changeServiceType = this.changeServiceType.bind(this);
        this.readURL = this.readURL.bind(this);
    }

    componentDidMount() {
        $.get('/LabResults/GetServiceTypesOfLabResult?id=' + serviceTypeID, (data) => {
            this.setState({
                serviceTypes: data
            });

            if (this.state.action.toLowerCase() != "create") {

                if (serviceTypeID != null && serviceTypeID != "" && serviceTypeID != undefined) {
                    var labresult = this.state.serviceTypes.filter(x => x.id == serviceTypeID);
                    if (labresult.length > 0) {
                        if (labresult[0].isLabReport) {
                            if (labResultID != null && labResultID != "" && labResultID != undefined) {
                                $.get('/LabResults/GetLabReport?id=' + labResultID, (data) => {
                                    this.setState({
                                        labResults: data
                                    });
                                });
                            }
                        } else {
                            if (labResultID != null && labResultID != "" && labResultID != undefined) {
                                $.get('/LabResults/GetLabResultServices?id=' + labResultID, (data) => {
                                    this.setState({
                                        labResults: data
                                    });
                                });
                            }
                        }
                        this.setState({ isLabReport: labresult[0].isLabReport });
                        this.setState({ reportBodyName: labresult[0].reportBodyName });
                    }
                }
            }
        });

        var methods = this;
        if (!$('#labResultTypeID').hasClass('select2-hidden-accessible')) {
            //console.log('do not have class select2');
            $('#labResultTypeID').select2();
            $('#labResultTypeID').on('select2:select', function (e) {
                //console.log('select2 has been selected.');
                methods.changeServiceType(e);
            });
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        //console.log('component did update');
        var methods = this;
        if (this.state.serviceTypeID !== prevState.serviceTypeID) {
            console.log('service type changes');
            if (!$('#labResultTypeID').hasClass('select2-hidden-accessible')) {
                //console.log('do not have class select2');
                $('#labResultTypeID').select2();
                $('#labResultTypeID').on('select2:select', function (e) {
                    //console.log('select2 has been selected.');
                    methods.changeServiceType(e);
                });
            }
        }
        $('#labResultTypeID').trigger('change');
    }

    changeServiceType(e) {
        //console.log('change service type function');
        var id = e.target.value;
        console.log('id: '+id);
        this.setState({ serviceTypeID: id });
        if (id != null && id != "" && id != undefined) {
            $('#labResultTypeID').val(id).trigger('change');
            console.log('lab: ' + $('#labResultTypeID').val());
            var labresult = this.state.serviceTypes.filter(x => x.id == id);
            console.log(labresult);
            this.setState({ labResults: [] });
            if (labresult.length > 0) {
                if (labresult[0].isLabReport) {
                    $.get('/LabResults/GetLabReport', (data) => {
                        this.setState({
                            labResults: data
                        });
                    });
                } else {
                    $.get('/LabResults/GetLabResults?id=' + id, (data) => {
                        this.setState({
                            labResults: data
                        });
                    });
                }
                this.setState({ isLabReport: labresult[0].isLabReport });
                this.setState({ reportBodyName: labresult[0].reportBodyName });
            }
        } else {
            this.setState({ labResults: [] });
            this.setState({ isLabReport: false });
        }
    }

    readURL(e) {
        var thisprops = this;
        var input = e.target;
        var url = input.files[0].name;
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();

            reader.onload = function (e) {
                console.log(input.id);
                console.log(e.target.result);
                if (input.id == 'LabPhoto1')
                    thisprops.state.labResults.labPhoto1 = e.target.result;
                else if (input.id == 'LabPhoto2')
                    thisprops.state.labResults.labPhoto2 = e.target.result;
                else if (input.id == 'LabPhoto3')
                    thisprops.state.labResults.labPhoto3 = e.target.result;
                thisprops.setState({ labResults: thisprops.state.labResults });
            };
            reader.readAsDataURL(input.files[0]);
        }
        else {
            alert('Please upload correct photo. Supported extensions: gif, png, jpeg, jpg.');
        }
    }

    render() {
        return (
            <div className='w-100'>
                <div className="form-group col-4">
                    <label className="control-label">Lab Result Type</label>
                    <input type='hidden' name='ServiceTypeID' value={this.state.serviceTypeID} />
                    <select className='form-control'
                        id='labResultTypeID'
                        value={this.state.serviceTypeID}
                        onChange={(e) => changeServiceType(e)}
                        disabled={this.state.action.toLowerCase() != 'create' ? 'disabled' : ''} >
                        <option value=''>Select Lab Result Type</option>
                        {
                            this.state.serviceTypes.map((serviceType, i) =>
                                <option key={i} value={serviceType.id}>{serviceType.name}</option>
                            )
                        }
                    </select>
                </div>
                {
                    this.state.isLabReport
                        ?
                        (
                            <React.Fragment>
                                <div className='form-group col-12'>
                                    <label>Gross</label>
                                    <textarea name={'Gross'} className='form-control'
                                        value={this.state.labResults.gross}
                                        onChange={(e) => {
                                            this.state.labResults.gross = e.target.value;
                                            this.setState({ labResults: this.state.labResults });
                                        }}>
                                    </textarea>
                                </div>
                                <div className='row col'>
                                    <div className='form-group col-4'>
                                        <input type='hidden' name={'LabPhoto1'} value={this.state.labResults.labPhoto1} />
                                        <div style={{ backgroundSize: 'contain', textAlign: 'center', cursor: 'pointer', maxWidth: '100%', maxHeight: '150px' }}>
                                            <img onClick={() => $(LabPhoto1).click()} src={(this.state.labResults.labPhoto1 != null && this.state.labResults.labPhoto1 != '') ? this.state.labResults.labPhoto1 : '/img/no-photo.png'} className='labphoto' height='150' />
                                        </div>
                                        <label className="form-group col-12 text-center">{this.state.reportBodyName} (Low Power)</label>
                                        <input type='file' id={'LabPhoto1'} className='form-control d-none' onChange={this.readURL} />
                                    </div>
                                    <div className='form-group col-4'>
                                        <input type='hidden' name={'LabPhoto2'} value={this.state.labResults.labPhoto2} />
                                        <div style={{ backgroundSize: 'contain', textAlign: 'center', cursor: 'pointer', maxWidth: '100%', maxHeight: '150px' }}>
                                            <img onClick={() => $(LabPhoto2).click()} src={(this.state.labResults.labPhoto2 != null && this.state.labResults.labPhoto2 != '') ? this.state.labResults.labPhoto2 : '/img/no-photo.png'} className='labphoto' height='150' />
                                        </div>
                                        <label className="form-group col-12 text-center">{this.state.reportBodyName} (High Power) 1</label>
                                        <input type='file' id={'LabPhoto2'} className='form-control d-none' onChange={this.readURL} />
                                    </div>
                                    <div className='form-group col-4'>
                                        <input type='hidden' name={'LabPhoto3'} value={this.state.labResults.labPhoto3} />
                                        <div style={{ backgroundSize: 'contain', textAlign: 'center', cursor: 'pointer', maxWidth: '100%', maxHeight: '150px' }}>
                                            <img onClick={() => $(LabPhoto3).click()} src={(this.state.labResults.labPhoto3 != null && this.state.labResults.labPhoto3 != '') ? this.state.labResults.labPhoto3 : '/img/no-photo.png'} className='labphoto' height='150' />
                                        </div>
                                        <label className="form-group col-12 text-center">{this.state.reportBodyName} (High Power) 2</label>
                                        <input type='file' id={'LabPhoto3'} className='form-control d-none' onChange={this.readURL} />
                                    </div>
                                </div>
                                <div className='form-group col-12'>
                                    <label>{this.state.reportBodyName}</label>
                                    <textarea name={'ReportBody'} className='form-control'
                                        value={this.state.labResults.reportBody}
                                        onChange={(e) => {
                                            this.state.labResults.reportBody = e.target.value;
                                            this.setState({ labResults: this.state.labResults });
                                        }}>
                                    </textarea>
                                </div>
                                {/* <div className='form-group col-12'>
                                    <label>Remark</label>
                                    <textarea name={'Remark'} className='form-control'
                                        value={this.state.labResults.remark}
                                        onChange={(e) => {
                                            this.state.labResults.remark = e.target.value;
                                            this.setState({ labResults: this.state.labResults });
                                        }}>
                                    </textarea>
                                </div> */}
                            </React.Fragment>
                        )
                        :
                        (
                            <div className='form-group col-12 table-responsive'>
                                <table className="table table-bordered table-condensed">
                                    <thead>
                                        <tr className="react-table-header">
                                            <th className='w-10'></th>
                                            <th className='text-center'>Test</th>
                                            <th className='text-center'>Result</th>
                                            <th className='text-center'>Unit</th>
                                            <th className='text-center'>Reference Range</th>
                                            <th className='text-center'>Remark</th>
                                        </tr>
                                    </thead >
                                    <tbody>
                                        {
                                            this.state.labResults.map((labResult, i) =>
                                                <tr key={i}>
                                                    <td>
                                                        <input type='checkbox' className='form-control mt-4 mr-3 w-20'
                                                            name={'LabResult_Services[' + i + '].PrintResult'}
                                                            value={labResult.printResult}
                                                            checked={labResult.printResult}
                                                            onChange={(e) => {
                                                                labResult.printResult = e.target.checked;
                                                                this.setState({ labResults: this.state.labResults });
                                                            }} />
                                                    </td>
                                                    <td className='pt-4'>
                                                        <input type='hidden' name={'LabResult_Services[' + i + '].ServiceID'} value={labResult.id} />
                                                        <input type='hidden' name={'LabResult_Services[' + i + '].LabUnit'} value={labResult.labUnit} />
                                                        <input type='hidden' name={'LabResult_Services[' + i + '].MinRange'} value={labResult.minRange} />
                                                        <input type='hidden' name={'LabResult_Services[' + i + '].MaxRange'} value={labResult.maxRange} />
                                                        <input type='hidden' name={'LabResult_Services[' + i + '].SelectList'} value={labResult.selectList} />
                                                        <input type='hidden' name={'LabResult_Services[' + i + '].Type'} value={labResult.type} />
                                                        <input type='hidden' name={'LabResult_Services[' + i + '].SortOrder'} value={labResult.sortOrder} />
                                                        {labResult.name}
                                                    </td>
                                                    {
                                                        labResult.type == 0 // Result Not Set Up
                                                            ?
                                                            (
                                                                <React.Fragment>
                                                                    <td>
                                                                        <input type='text' className='form-control'
                                                                            name={'LabResult_Services[' + i + '].Result'}
                                                                            value={labResult.result}
                                                                            onChange={(e) => {
                                                                                var result = e.target.value;
                                                                                labResult.result = result;
                                                                                this.setState({ labResults: this.state.labResults });
                                                                            }} />
                                                                    </td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </React.Fragment>
                                                            )
                                                            :
                                                            (
                                                                labResult.type == 1 // Range
                                                                    ?
                                                                    (
                                                                        <React.Fragment>
                                                                            <td>
                                                                                <input type='text' className='form-control'
                                                                                    name={'LabResult_Services[' + i + '].Result'}
                                                                                    //step='0.01'
                                                                                    value={labResult.result}
                                                                                    onChange={(e) => {
                                                                                        var result = e.target.value;
                                                                                        labResult.result = result;
                                                                                        this.setState({ labResults: this.state.labResults });
                                                                                    }} />
                                                                            </td>
                                                                            <td className='text-center'>{labResult.labUnit}</td>
                                                                            <td className='text-center'>{((labResult.minRange != null) ? labResult.minRange : '') + ' ~ ' + ((labResult.maxRange != null) ? labResult.maxRange : '')}</td>
                                                                        </React.Fragment>
                                                                    )
                                                                    :
                                                                    (
                                                                        labResult.type == 2 // Select
                                                                            ?
                                                                            (
                                                                                <React.Fragment>
                                                                                    <td>
                                                                                        <select className='form-control'
                                                                                            name={'LabResult_Services[' + i + '].Result'}
                                                                                            value={labResult.result}
                                                                                            onChange={(e) => {
                                                                                                var result = e.target.value;
                                                                                                labResult.result = result;
                                                                                                this.setState({ labResults: this.state.labResults });
                                                                                            }}>
                                                                                            <option value=""></option>
                                                                                            {
                                                                                                labResult.selectList.split('/').map((selectList, j) =>
                                                                                                    <option key={j} value={selectList}>{selectList}</option>
                                                                                                )
                                                                                            }
                                                                                        </select>
                                                                                    </td>
                                                                                    <td className='text-center'>{labResult.labUnit}</td>
                                                                                    <td></td>
                                                                                </React.Fragment>
                                                                            )
                                                                            :
                                                                            (
                                                                                <React.Fragment></React.Fragment>
                                                                            )
                                                                    )
                                                            )
                                                    }
                                                    <td>
                                                        <input type='text' className='form-control'
                                                            name={'LabResult_Services[' + i + '].Remark'}
                                                            value={labResult.remark}
                                                            onChange={(e) => {
                                                                labResult.remark = e.target.value;
                                                                this.setState({ labResults: this.state.labResults });
                                                            }} />
                                                    </td>
                                                </tr>
                                            )
                                        }
                                    </tbody>
                                </table >
                            </div >
                        )
                }
            </div>
        )
    }
}

ReactDOM.render(
    <LabResult ref={(component) => window.LabResultComponent = component} />, document.getElementById('root'));