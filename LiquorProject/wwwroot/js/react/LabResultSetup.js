﻿class LabResultSetup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            services: []
        };

        this.sortServices = this.sortServices.bind(this);
    }

    componentDidMount() {
        $.get('/Services/GetLabResultServices?id=' + serviceTypeID, (data) => {
            this.setState({
                services: data
            });
        });
    }

    sortServices() {
        this.state.services.sort(function (a, b) { return (parseInt(a.sortOrder) > parseInt(b.sortOrder)) ? 1 : ((parseInt(b.sortOrder) > parseInt(a.sortOrder)) ? -1 : 0); });
        this.setState({ services: this.state.services });
    }

    render() {
        return (
            <div>
                {/*<button type='button' className='btn sticky' onClick={this.sortServices}>Sort</button>*/}
                {
                    this.state.services.map((service, i) =>
                        <div className='row' key={i}>
                            <div className='col-md-2 form-inline'>
                                <div className='form-group'>
                                    <input type='checkbox' className='form-control mt-4 mr-3 w-20'
                                        name={'services[' + i + '].LabResultIsActive'}
                                        value={service.labResultIsActive}
                                        checked={service.labResultIsActive}
                                        onChange={(e) => { 
                                            service.labResultIsActive = e.target.checked; 
                                            this.setState({ services: this.state.services });
                                        }} />
                                    <input type='number' className='form-control mt-4 w-50'
                                        name={'services[' + i + '].SortOrder'}
                                        value={service.sortOrder}
                                        onChange={(e) => {
                                            service.sortOrder = e.target.value;
                                            this.setState({ services: this.state.services });
                                        }} />
                                </div>
                            </div>
                            <div className='col-md-2'>
                                <div className='form-group'>
                                    <input type='hidden' name={'services[' + i + '].ID'} value={service.id} />
                                    <h5 className='ml-3 mt-4 pt-2'>{service.name}</h5>
                                </div>
                            </div>
                            <div className='col-md-3'>
                                <div className="form-group">
                                    <select className="form-control mt-4"
                                        name={'services[' + i + '].Type'}
                                        value={service.type}
                                        onChange={(e) => {
                                            service.type = e.target.value;
                                            this.setState({
                                                services: this.state.services
                                            });
                                        }} >
                                        <option value='0'>No Result Setup</option>
                                        <option value='1'>Range</option>
                                        <option value='2'>Select</option>
                                    </select>
                                </div>
                            </div>
                            <div className='col-md-5 form-inline'>
                                <div className='form-group row w-100'>
                                    {
                                        service.type == 1 // Range
                                            ?
                                            (
                                                <div className='mt-2 col-12'>
                                                    <input type='number' step='0.01' className='form-control col-3 mr-2' placeholder='From' title="from"
                                                        name={'services[' + i + '].MinRange'}
                                                        value={service.minRange}
                                                        onChange={(e) => {
                                                            service.minRange = e.target.value;
                                                            this.setState({ service: this.state.service });
                                                        }} />
                                                    ~
                                                    <input type='number' step='0.01' className='form-control col-3 ml-2' min={service.min} placeholder='To' title="to"
                                                        name={'services[' + i + '].MaxRange'}
                                                        value={service.maxRange}
                                                        onChange={(e) => {
                                                            service.maxRange = e.target.value;
                                                            this.setState({ service: this.state.service });
                                                        }} />
                                                    <input type='text' className='form-control col-3 ml-2' placeholder='Unit'
                                                        name={'services[' + i + '].LabUnit'}
                                                        value={service.labUnit}
                                                        onChange={(e) => {
                                                            service.labUnit = e.target.value;
                                                            this.setState({ service: this.state.service });
                                                        }} />
                                                </div>
                                            )
                                            : (service.type == 2 // Select
                                                ?
                                                (
                                                    <div className='mt-2 col-12'>
                                                        <input type='text' className='form-control w-50' required placeholder='Please seperate items with "/"'
                                                            name={'services[' + i + '].SelectList'}
                                                            value={service.selectList}
                                                            onChange={(e) => {
                                                                service.selectList = e.target.value;
                                                                this.setState({ service: this.state.service });
                                                            }} />
                                                        <input type='text' className='form-control col-3  ml-2' placeholder='Unit'
                                                            name={'services[' + i + '].LabUnit'}
                                                            value={service.labUnit}
                                                            onChange={(e) => {
                                                                service.labUnit = e.target.value;
                                                                this.setState({ service: this.state.service });
                                                            }} />
                                                    </div>
                                                )
                                                : (<div></div>))
                                    }
                                </div>
                            </div>
                        </div>
                    )
                }
            </div>

        )
    }
}

ReactDOM.render(
    <LabResultSetup ref={(component) => window.LabResultSetupComponent = component} />, document.getElementById('root'));